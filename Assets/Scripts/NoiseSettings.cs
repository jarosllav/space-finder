using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Noise setting", order = 2)]
public class NoiseSettings : ScriptableObject
{
    public float Scale;
    public int Octaves;
    public float Lacunarity;
    public float Persistence;
}
