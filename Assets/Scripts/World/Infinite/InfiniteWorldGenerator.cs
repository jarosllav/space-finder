using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

public class InfiniteWorldGenerator
{
	// Private fields
	//private PoissonDiscSampling poissonDisc;
    private VoronoiBiome biomeGenerator;
	private ChunkGenerator chunkGenerator;

	private int seed;

	private Vector2 worldSize;
	private Vector3 chunkSize;
	private Material chunkMaterial;
    
	private BiomeSettings[] biomeTypes;
	private List<Biome> biomes;
	//private float threshold;

	private NativeArray<float> scales;
	private NativeArray<float> lacunarities;
	private NativeArray<float> persistencies;
	private NativeArray<int> octaves;

	private List<ChunkJobData> scheduledChunks; //TODO: implement async

	private List<Vector2Int> resourcesChunks;
	private List<Vector2> resourcesPositions;
	private Vector2 resourcesChunkSize;

	private bool generatedNewChunks = false;

    public InfiniteWorldGenerator(int seed, Vector2 worldSize, Vector3 chunkSize, Material chunkMaterial, BiomeSettings[] biomeTypes) {
		this.seed = seed;
		this.worldSize = worldSize;
        this.chunkSize = chunkSize;
		this.biomeTypes = biomeTypes;
		this.chunkMaterial = chunkMaterial;

		this.scheduledChunks = new List<ChunkJobData>();
		this.resourcesChunks = new List<Vector2Int>();
		this.resourcesPositions = new List<Vector2>();
		this.resourcesChunkSize = new Vector2(worldSize.x * chunkSize.x, worldSize.y * chunkSize.z);

		UnityEngine.Random.InitState(seed);

		this.chunkGenerator = new ChunkGenerator(this.chunkMaterial, this.chunkSize);
		this.biomeGenerator = new VoronoiBiome(new Vector2Int((int)chunkSize.x * 4, (int)chunkSize.z * 4));
    }

    // Public methods
	public void Destroy() {
		this.scales.Dispose();
		this.lacunarities.Dispose();
		this.persistencies.Dispose();
		this.octaves.Dispose();
	}

	public void Update(Vector2Int viewerCoords) {
		this.checkResourcesChunk(viewerCoords);

		// If generated new chunks check if resources can be placed on chunks 
		if(this.generatedNewChunks) {
			if(this.resourcesPositions.Count > 0) {
				this.processResources();
			}
		}

		this.generatedNewChunks = false;
	}

	/// <summary>
	/// Generates piece of world chunks (biomes, chunks, resources)
	/// </summary>
	/// <returns>Generated chunks list</returns>
	public List<Chunk> Generate() {
		this.prepare();

		List<Chunk> chunks = new List<Chunk>();

		// Biomes level
		for(int x = -3; x < 3; ++x) {
			for(int y = -3; y < 3; ++y) {
				this.biomeGenerator.Generate(new Vector2Int(x, y), (int)(this.chunkSize.x), this.biomeTypes);
			}
		}

		// Terrain level
		for(int x = 0; x < this.worldSize.x; ++x) {
			for(int y = 0; y < this.worldSize.y; ++y) {
				Vector2Int coords = new Vector2Int(x, y);

				Chunk chunk = this.GenerateChunk(coords);
				chunks.Add(chunk);
			}
		}

		// Resources level
		this.GenerateResourceChunk(Vector2Int.zero);

		#if UNITY_EDITOR
			if(!Application.isPlaying)
				this.Destroy();
		#endif

		return chunks;
	}

	/// <summary>
	/// Generates new chunk at given coordinates.
	/// </summary>
	/// <param name="coords">Chunk coordinates</param>
	/// <param name="lod">Level of detail</param>
	/// <param name="threshold">Mixing thershold between biomes</param>
	/// <param name="surfaceLevel">Surface level for marching cubes</param>
	/// <returns>New chunk object</returns>
    public Chunk GenerateChunk(Vector2Int coords, int lod = 0, float threshold = 0.5f, float surfaceLevel = 0.5f) {
		this.checkVoronoiChunks(coords);

		float3 chunkPosition = new float3(this.chunkSize.x * coords.x, 0, this.chunkSize.z * coords.y);
		List<Biome> biomesList = this.biomeGenerator.GetNeighbourBiomes(chunkPosition);

		ChunkJob chunkJob = new ChunkJob(this.biomeTypes, biomesList, chunkPosition, this.chunkSize, threshold);

		// Noise
		chunkJob.Seed = this.seed;
		chunkJob.Scales = this.scales;
		chunkJob.Lacunarities = this.lacunarities;
		chunkJob.Persistances = this.persistencies;
		chunkJob.Octaves = this.octaves;
		chunkJob.GradientThreshold = threshold;

		// Marching
		chunkJob.LevelOfDetail = lod;
		chunkJob.SurfaceLevel = surfaceLevel;

		chunkJob.Schedule();
		//chunkJob.Complete();

		Chunk chunk = this.chunkGenerator.Generate(coords, chunkPosition, chunkJob.GetVertices(), chunkJob.GetTriangles(), chunkJob.GetColors(), chunkJob.GetNoise());

		chunkJob.Dispose();

		this.generatedNewChunks = true;

		return chunk;
	}

	/// <summary>
	/// Generates new chunk at given coordinates and noise values (used for load-system)
	/// </summary>
	/// <param name="coords">Chunk coordinates</param>
	/// <param name="noise">Noise values for vertices</param>
	/// <param name="lod">Level of detail</param>
	/// <param name="threshold">Mixing thershold between biomes</param>
	/// <param name="surfaceLevel">Surface level for marching cubes</param>
	/// <returns>New chunk object</returns>
	public Chunk GenerateChunkWithNoise(Vector2Int coords, float[] noise, int lod = 0, float threshold = 0.5f, float surfaceLevel = 0.5f) {
		float3 chunkPosition = new float3(this.chunkSize.x * coords.x, 0, this.chunkSize.z * coords.y);
		List<Biome> biomesList = this.biomeGenerator.GetNeighbourBiomes(chunkPosition);

		ChunkJob chunkJob = new ChunkJob(this.biomeTypes, biomesList, chunkPosition, this.chunkSize, threshold);
		chunkJob.Seed = this.seed;
		chunkJob.LevelOfDetail = lod;
		chunkJob.SurfaceLevel = surfaceLevel;

		chunkJob.ScheduleWithNoise(noise);
		//chunkJob.Complete();

		Chunk chunk = this.chunkGenerator.Generate(coords, chunkPosition, chunkJob.GetVertices(), chunkJob.GetTriangles(), chunkJob.GetColors(), chunkJob.GetNoise());
		chunkJob.Dispose();
		return chunk;
	}

	/// <summary>
	/// Regenerates given chunk mesh
	/// </summary>
	/// <param name="chunk">Chunk to regenerate</param>
	public void RegenerateChunk(Chunk chunk) {
		Vector2Int coords = chunk.GetCoords();
		float3 chunkPosition = new float3(this.chunkSize.x * coords.x, 0, this.chunkSize.z * coords.y);
		List<Biome> biomesList = this.biomeGenerator.GetNeighbourBiomes(chunkPosition);

		ChunkJob chunkJob = new ChunkJob(this.biomeTypes, biomesList, chunkPosition, this.chunkSize, 0.5f);
		chunkJob.Seed = this.seed;
		chunkJob.LevelOfDetail = 0;
		chunkJob.SurfaceLevel = 0.5f;

		chunkJob.ScheduleWithNoise(chunk.Data.Noise);
		//chunkJob.Complete();

		chunkGenerator.Regenerate(chunk, chunkJob.GetVertices(), chunkJob.GetTriangles(), chunkJob.GetColors());
		chunkJob.Dispose();
	}

	/// <summary>
	/// Generates new points for resources
	/// </summary>
	/// <param name="coords">Resource chunk coordinates</param>
	/// <param name="density">Points density</param>
	public void GenerateResourceChunk(Vector2Int coords, float density = 10) {
		Vector2 offset = this.resourcesChunkSize * coords;
		List<Vector2> points = PoissonDiscSampling.Generate(density, this.resourcesChunkSize, 15);
		points.ForEach(p => this.resourcesPositions.Add(p + offset));
	}

	public List<WorldResource> GenerateResources() {
		/*if(this.resourcesPositions.Count <= 0)
			return null;

		List<WorldResource> resources = new List<WorldResource>();

		for(int i = 0; i < this.resourcesPositions.Count; ++i) {
			RaycastHit hit;

			Vector3 origin = new Vector3(this.resourcesPositions[i].x, chunkSize.y + 1, this.resourcesPositions[i].y);
			if(Physics.Raycast(origin, Vector3.down, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Terrain"))) {
				BiomeSettings biome = this.GetBiome(hit.point);
				
				if(biome.Resources.Length > 0) {
					GameObject resourceObj = GameObject.Instantiate(biome.Resources[UnityEngine.Random.Range(0, biome.Resources.Length)], hit.point, Quaternion.identity);
					resourceObj.transform.parent = GameObject.Find("_Dynamic").transform;

					WorldResource resource = resourceObj.GetComponent<WorldResource>();
					resources.Add(resource);
				}
			}

			this.resourcesPositions.RemoveAt(i);
		}*/

		return null;
	}	

	public void SetBiomeTypes(BiomeSettings[] biomeTypes) { this.biomeTypes = biomeTypes; }
	public void SetSeed(int seed) { this.seed = seed; }

	public BiomeSettings GetBiome(Vector3 position) {
		Biome centoroid = biomeGenerator.GetClosestBiome(position);
		return centoroid.BiomeSettings;
	}
	public List<Biome> GetBiomes() { return this.biomes; }
	public Vector2 GetWorldSize() { return this.worldSize; }
	public Vector3 GetChunkSize() { return this.chunkSize; }
	public Material GetChunkMaterial() { return this.chunkMaterial; }
	public BiomeSettings[] GetBiomeTypes() { return this.biomeTypes; }
	public VoronoiBiome GetBiomeGenerator() { return this.biomeGenerator; }

    // Private methods
	
	/// <summary>
	/// Prepares persistent data for world generation
	/// </summary>
	private void prepare() {
		this.biomeGenerator.Clear();
		this.resourcesChunks.Clear();

		this.scales = new NativeArray<float>(this.biomeTypes.Length, Allocator.Persistent);
		this.lacunarities = new NativeArray<float>(this.biomeTypes.Length, Allocator.Persistent);
		this.persistencies = new NativeArray<float>(this.biomeTypes.Length, Allocator.Persistent);
		this.octaves = new NativeArray<int>(this.biomeTypes.Length, Allocator.Persistent);

		for(int i = 0; i < this.biomeTypes.Length; ++i) {
			NoiseSettings biomeSettings = this.biomeTypes[i].NoiseSettings;

			this.scales[i] = biomeSettings.Scale;
			this.lacunarities[i] = biomeSettings.Lacunarity;
			this.persistencies[i] = biomeSettings.Persistence;
			this.octaves[i] = biomeSettings.Octaves;
		}
	}

	/// <summary>
	/// Checks if voronoi chunk exists on given chunk terrain coords and generates new if not exists.
	/// </summary>
	/// <param name="coords">Chunk coord</param>
    private void checkVoronoiChunks(Vector2Int coords) {
		Vector2Int chunksPerVoronoi = new Vector2Int((int)(biomeGenerator.GetSize().x / chunkSize.x), (int)(biomeGenerator.GetSize().y / chunkSize.z));
		Vector2Int voronoiCoords = new Vector2Int(coords.x / chunksPerVoronoi.x, coords.y / chunksPerVoronoi.y);

		for(int x = -1; x <= 1; ++x) {
			for(int y = -1; y <= 1; ++y) {
				if(!biomeGenerator.FindChunk(voronoiCoords + new Vector2Int(x, y))) {
					biomeGenerator.Generate(voronoiCoords + new Vector2Int(x, y), (int)(this.chunkSize.x), this.biomeTypes);
				}
			}
		}
	}

	/// <summary>
	/// Iterates stored resources positions and checks if can be placed on chunk.
	/// If raycast find a chunk at position then instantiate new resource object and assign it to chunk.
	/// </summary>
	private void processResources() {
		int i = 0; 
		int layerMask = 1 << LayerMask.NameToLayer("Terrain");
		while(i < this.resourcesPositions.Count) {
			Vector2 position = this.resourcesPositions[i];
			Vector3 origin = new Vector3(position.x, chunkSize.y + 1, position.y);
			
			RaycastHit hit;
			if(Physics.Raycast(origin, Vector3.down, out hit, Mathf.Infinity, layerMask)) {
				if(hit.collider.tag == "Chunk") {
					BiomeSettings biome = this.GetBiome(hit.point);
					if(biome.Resources.Length > 0) {
						Chunk chunk = hit.collider.GetComponent<Chunk>();
						if(chunk != null) {
							GameObject resourceObj = GameObject.Instantiate(biome.Resources[UnityEngine.Random.Range(0, biome.Resources.Length)], hit.point, Quaternion.identity);
							resourceObj.transform.parent = chunk.transform;

							WorldResource resource = resourceObj.GetComponent<WorldResource>();
							chunk.Resources.Add(resource);
						}
					}

					this.resourcesPositions.RemoveAt(i);
				}
			}
			else {
				++i;
			}
		}
	}

	/// <summary>
	/// Generates new resource chunk if viewer is close enough
	/// </summary>
	/// <param name="viewerCoords">Viewer chunk coordinate</param>
	private void checkResourcesChunk(Vector2Int viewerCoords) {
		Vector2Int chunksInResourceChunk = new Vector2Int((int)(this.resourcesChunkSize.x / this.chunkSize.x), (int)(this.resourcesChunkSize.y / this.chunkSize.z));
		Vector2Int resourceCoords = new Vector2Int(viewerCoords.x / chunksInResourceChunk.x, viewerCoords.y / chunksInResourceChunk.y);

		if(!this.resourcesChunks.Contains(resourceCoords)) {
			this.resourcesChunks.Add(resourceCoords);
			this.GenerateResourceChunk(resourceCoords);
		}
	}
}
	