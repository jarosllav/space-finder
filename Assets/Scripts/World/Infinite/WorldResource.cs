﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldResource : MonoBehaviour
{
    // Public fields
    public string Name = "unknown";
    public int Health = 3;
    public bool IsDestroyed = false;
    public Item DropItem;
    public int DropAmount;

    // Unity methods
    private void Awake() {
        // ...
    }

    // Public methods
    
    /// <summary>
    /// Deals damage to world resource object
    /// </summary>
    /// <param name="damage">Strength of hit</param>
    public void Hit(int damage) {
        this.Health -= damage;
        if(this.Health <= 0) {
            IsDestroyed = true;
            Destroy(this.gameObject);
        }
    }
}
