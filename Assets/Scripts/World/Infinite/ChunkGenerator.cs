using System.Collections.Generic;
using UnityEngine;

public class ChunkGenerator {
    // Private fields
    private Material material;
    private Vector3 chunkSize;
    
    public ChunkGenerator(Material material, Vector3 chunkSize) {
        this.material = material;
        this.chunkSize = chunkSize;
    }

    // Public methods

    /// <summary>
    /// Generates new chunk GameObject (with mesh) 
    /// </summary>
    /// <param name="coords">Chunk coordinates</param>
    /// <param name="position">Chunk position</param>
    /// <param name="vertices">Vertices array</param>
    /// <param name="triangles">Triangles array</param>
    /// <param name="colors">Colors array</param>
    /// <param name="noise">Noise values array</param>
    /// <returns>Chunk instance</returns>
    public Chunk Generate(Vector2Int coords, Vector3 position, Vector3[] vertices, int[] triangles, Color32[] colors, float[] noise) {
        GameObject chunkObject = new GameObject("Chunk " + coords);

        MeshFilter meshFilter = chunkObject.AddComponent<MeshFilter>();
        MeshRenderer meshRenderer = chunkObject.AddComponent<MeshRenderer>();

        meshRenderer.sharedMaterial = material;

        Mesh mesh = this.createChunkMesh(vertices, triangles, colors);
        meshFilter.mesh = mesh;

        chunkObject.AddComponent<MeshCollider>();
        chunkObject.transform.position = position;
        chunkObject.layer = 9;
        chunkObject.tag = "Chunk";

        Chunk chunk = chunkObject.AddComponent<Chunk>();
        chunk.SetCoords(coords);
        chunk.SetSize(this.chunkSize);
        chunk.SetPosition(position);

        chunk.Data = new ChunkData(vertices, triangles, colors, noise);

        return chunk;
    }

    /// <summary>
    /// Regenerates chunk object (creates new mesh and update chunk data)
    /// </summary>
    /// <param name="chunk">Chunk instance</param>
    /// <param name="vertices">New vertices array</param>
    /// <param name="triangles">New triangles array</param>
    /// <param name="colors">New colors array</param>
    public void Regenerate(Chunk chunk, Vector3[] vertices, int[] triangles, Color32[] colors) {
        Mesh mesh = this.createChunkMesh(vertices, triangles, colors);

        chunk.gameObject.GetComponent<MeshFilter>().mesh = mesh;
        chunk.gameObject.GetComponent<MeshCollider>().sharedMesh = mesh;

        chunk.Data.Vertices = vertices;
        chunk.Data.Triangles = triangles;
        chunk.Data.Colors = colors;
    }
    
    // Private methods

    /// <summary>
    /// Creating new Mesh instance and assing data to it.
    /// </summary>
    /// <param name="vertices">Mesh vertices</param>
    /// <param name="triangles">Mesh triangles</param>
    /// <param name="colors">Mesh vertices colors</param>
    /// <returns>Mesh instance</returns>
    private Mesh createChunkMesh(Vector3[] vertices, int[] triangles, Color32[] colors) {
        Mesh mesh = new Mesh();
        mesh.name = "Chunk";

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.colors32 = colors;
        mesh.RecalculateNormals();

        return mesh;
    }
}