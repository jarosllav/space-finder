using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

#if UNITY_EDITOR
using UnityEditor;
using System.Reflection;
#endif

public class InfiniteWorld : MonoBehaviour, IWorld
{
	// Private fields
	[Header("Generation settings")]
	[SerializeField] private int seed;
	[SerializeField] private BiomeSettings[] biomeTypes;
	[SerializeField] private Vector2 regionSize;
	[SerializeField] private Vector3 chunkSize;
	[SerializeField] private Material chunkMaterial;
	[SerializeField] private float biomeDensity;
	[SerializeField] private float resourcesDensity;

	[Header("Gameplay settings")]
	[SerializeField] private Transform viewerTransform;
	[SerializeField] private float viewDistance = 100f;

	[Header("Debug")]
	[SerializeField] private bool debugGenerateChunks = true;

    private InfiniteWorldGenerator worldGenerator;

    private Dictionary<Vector2Int, Chunk> chunks = new Dictionary<Vector2Int, Chunk>();
	private List<Chunk> activeChunks = new List<Chunk>();
	private HashSet<Chunk> modifiedChunks = new HashSet<Chunk>();

    // Unity methods
    private void Awake() {
		// ...
    }

	private void OnDestroy() {
		this.worldGenerator?.Destroy();
	}

    private void Update() {
        if(this.viewerTransform != null) {
			Vector2Int chunksInView = new Vector2Int((int)(this.viewDistance / this.chunkSize.x), (int)(this.viewDistance / this.chunkSize.z));
			Vector2Int viewerCoords = new Vector2Int(
				Mathf.FloorToInt(this.viewerTransform.position.x / this.chunkSize.x), 
				Mathf.FloorToInt(this.viewerTransform.position.z / this.chunkSize.z));

			this.clearActiveChunks();

			for(int x = 0; x < chunksInView.x; ++x) {
				for(int y = 0; y < chunksInView.y; ++y) {
					Vector2Int coords = new Vector2Int(viewerCoords.x + x - chunksInView.x / 2, viewerCoords.y + y - chunksInView.y / 2);

					if(this.chunks.ContainsKey(coords)) {
						this.activeChunks.Add(this.chunks[coords]);
						this.chunks[coords]?.gameObject.SetActive(true);
					}
					else {
						if(this.debugGenerateChunks) {
							Chunk chunk = this.worldGenerator.GenerateChunk(coords);

							this.chunks.Add(coords, chunk);
							this.activeChunks.Add(chunk);

							chunk.transform.parent = this.transform;
							chunk.gameObject.SetActive(true);
						}
					}
				}
			}

			this.worldGenerator.Update(viewerCoords);
		}
    }

    // Public methods

	/// <summary>
	/// Initlize world and world generator.
	/// </summary>
	/// <param name="seed"></param>
	public void Init(int seed) {
		this.seed = seed;
		this.worldGenerator = new InfiniteWorldGenerator(seed, regionSize, chunkSize, chunkMaterial, biomeTypes);
	}

	/// <summary>
	/// Cleanup chunks, pregenerate piece of world and set player position to map center.
	/// </summary>
    public void Generate() {
		this.chunks.Clear();
		this.activeChunks.Clear();

		#if UNITY_EDITOR
		this.worldGenerator = new InfiniteWorldGenerator(seed, regionSize, chunkSize, chunkMaterial, biomeTypes);
		#endif
		
		List<Chunk> worldChunks = worldGenerator.Generate();
		foreach(Chunk chunk in worldChunks) {
			chunk.transform.parent = this.transform;
			
			this.chunks.Add(chunk.GetCoords(), chunk);
			this.activeChunks.Add(chunk);
		}

		if(this.viewerTransform) {
			Vector3 spawnPosition = new Vector3((this.chunkSize.x / 2) * this.regionSize.x, this.chunkSize.y + 2, 
				(this.chunkSize.z / 2) * this.regionSize.y);

			RaycastHit hit;
			if(Physics.Raycast(spawnPosition, -Vector3.up, out hit)) {
				spawnPosition = hit.point;
				spawnPosition.y += 5f;
			}

			CharacterController controller = this.viewerTransform.GetComponent<CharacterController>();
			if(controller != null)
				controller.enabled = false;

			this.viewerTransform.transform.position = spawnPosition;
		
			if(controller != null)
				controller.enabled = true;
		}
	}

	/// <summary>
	/// Loads chunks data from save
	/// </summary>
	/// <param name="chunkSerializables">Chunks data</param>
	public void LoadFromSave(List<ChunkSerializable> chunkSerializables) {
		foreach(ChunkSerializable chunkSerializable in chunkSerializables) {
			Vector2Int coords = new Vector2Int(chunkSerializable.X, chunkSerializable.Y);

			if(this.chunks.ContainsKey(coords)) {
				this.chunks[coords].Data.Noise = chunkSerializable.Noise;
				this.worldGenerator.RegenerateChunk(this.chunks[coords]);

			}
			else {
				Chunk chunk = this.worldGenerator.GenerateChunkWithNoise(coords, chunkSerializable.Noise);
				chunk.transform.parent = this.transform;
				chunk.gameObject.SetActive(true);

				this.chunks.Add(coords, chunk);
				this.activeChunks.Add(chunk);
			}
		}
	}

	/// <summary>
	/// Removes terrain at given position and brush radius
	/// </summary>
	/// <param name="point">World position</param>
	/// <param name="radius">Brush radius</param>
	public void LowerTerrain(Vector3 point, int radius = 1) {
		HashSet<Chunk> overlaping = this.getOverlapingChunks(point, radius);

		foreach(Chunk overlapChunk in overlaping) {
			overlapChunk.LowerTerrain(point, radius);
			this.worldGenerator.RegenerateChunk(overlapChunk);
			this.modifiedChunks.Add(overlapChunk);
		}
	}

	/// <summary>
	/// Adds terrain at given position and brush radius
	/// </summary>
	/// <param name="point">World position</param>
	/// <param name="radius">Brush radius</param>
	public void HigherTerrain(Vector3 point, int radius = 1) {
		HashSet<Chunk> overlaping = this.getOverlapingChunks(point, radius);
		
		foreach(Chunk overlapChunk in overlaping) {
			overlapChunk?.HigherTerrain(point, radius);
			this.worldGenerator.RegenerateChunk(overlapChunk);
			this.modifiedChunks.Add(overlapChunk);
		}
	}

	/// <summary>
	/// Returns new position snapped to terrain mesh.
	/// TODO: That method could check if there are space for object
	/// </summary>
	/// <param name="position">Origin world position</param>
	/// <returns>Snapped vector to terrain mesh (if raycast can't hit terrain returns Vector3(0, 0, 0))</returns>
	public Vector3 SnapToTerrain(Vector3 position) {
		Vector3 origin = new Vector3(0f, this.chunkSize.y, 0f) + position;

        RaycastHit[] hits = Physics.RaycastAll(origin, Vector3.down, float.PositiveInfinity, 1 << LayerMask.NameToLayer("Terrain"));

        if(hits.Length > 0) {
            RaycastHit hit = hits.Last();
			return hit.point;
		}

		return Vector3.zero;
	}

	/// <summary>
	/// Returns chunk in which a position is located.
	/// </summary>
	/// <param name="position">World position</param>
	/// <returns>Reference to Chunk (if chunk doesnt exists returns null)</returns>
	public Chunk GetChunk(Vector3 position) {
		Vector2Int chunkCoords = new Vector2Int((int)(position.x / chunkSize.x), (int)(position.z / chunkSize.z));
		if(chunks.ContainsKey(chunkCoords)) {
			return chunks[chunkCoords];
		}
		return null;
	}

	public void ClearModifiedChunks() { this.modifiedChunks.Clear(); }

	public int GetSeed() { return this.seed; }
	public Vector3 GetChunkSize() { return this.chunkSize; }
	public Vector3 GetRegionSize() { return this.regionSize; }
	public Material GetChunkMaterial() { return this.chunkMaterial; }
	public BiomeSettings[] GetBiomeTypes() { return this.biomeTypes; }
	public HashSet<Chunk> GetModifiedChunks() { return this.modifiedChunks; }
	public InfiniteWorldGenerator GetWorldGenerator() { return this.worldGenerator; }

    // Private methods
	
	/// <summary>
	/// Unactive all chunks in active list
	/// </summary>
	private void clearActiveChunks() {
		for(int i = 0; i < this.activeChunks.Count; ++i)
			this.activeChunks[i]?.gameObject.SetActive(false);
		this.activeChunks.Clear();
	}

	private void updateChunkLod(Chunk chunk) {
		// Not implemented yet
		/*Vector2 viewerPosition = new Vector2(viewerTransform.transform.position.x, viewerTransform.transform.position.z);
		for(int i = 0; i < activeChunks.Count; ++i) {
			int levelOfDetail = 0;
			Vector2 chunkPosition = new Vector2(chunk.GetPosition().x, chunk.GetPosition().z);

			float dst = Vector2.Distance(chunkPosition, viewerPosition);
			if(dst > viewDistance / 1.25f)
				levelOfDetail = 2;
			else if(dst > viewDistance / 1.5f)
				levelOfDetail = 1;
			else 
				levelOfDetail = 0;

			if(chunk.GetLOD() != levelOfDetail)
				chunk.SetLOD(levelOfDetail, worldGenerator.GetVoronoi());
		}*/
	}

	private HashSet<Chunk> getOverlapingChunks(Vector3 point, int radius) {
		//TODO: I have no idea how to do this in other way. But works for now.
		float pointX = Mathf.Round(point.x);
		float pointZ = Mathf.Round(point.z);

		HashSet<Chunk> overlaping = new HashSet<Chunk>();

		Vector2Int coords = new Vector2Int(Mathf.FloorToInt((pointX) / this.chunkSize.x), Mathf.FloorToInt((pointZ) / this.chunkSize.z));
		if(this.chunks.ContainsKey(coords))
			overlaping.Add(this.chunks[coords]);

		coords.x = Mathf.FloorToInt((pointX) / (this.chunkSize.x + 1));
		coords.y = Mathf.FloorToInt((pointZ) / (this.chunkSize.z + 1));
		if(this.chunks.ContainsKey(coords))
			overlaping.Add(this.chunks[coords]);

		coords.x = Mathf.FloorToInt((pointX) / (this.chunkSize.x));
		coords.y = Mathf.FloorToInt((pointZ) / (this.chunkSize.z + 1));
		if(this.chunks.ContainsKey(coords))
			overlaping.Add(this.chunks[coords]);

		coords.x = Mathf.FloorToInt((pointX) / (this.chunkSize.x + 1));
		coords.y = Mathf.FloorToInt((pointZ) / (this.chunkSize.z));
		if(this.chunks.ContainsKey(coords))
			overlaping.Add(this.chunks[coords]);

		for(int x = -radius; x < radius; ++x) {
			for(int z = -radius; z < radius; ++z) {
				int pX = (int)(pointX % this.chunkSize.x) + x; 
				int pZ = (int)(pointZ % this.chunkSize.z) + z; 

				if(pX <= 0 || pX >= this.chunkSize.x || pZ <= 0 || pZ >= this.chunkSize.z) {
					coords.x = Mathf.FloorToInt((pointX + x) / this.chunkSize.x);
					coords.y = Mathf.FloorToInt((pointZ + z) / this.chunkSize.z);
					if(this.chunks.ContainsKey(coords))
						overlaping.Add(this.chunks[coords]);

					coords.x = Mathf.FloorToInt((pointX + x) / (this.chunkSize.x + 1));
					coords.y = Mathf.FloorToInt((pointZ + z) / (this.chunkSize.z + 1));
					if(this.chunks.ContainsKey(coords))
						overlaping.Add(this.chunks[coords]);

					coords.x = Mathf.FloorToInt((pointX + x) / (this.chunkSize.x));
					coords.y = Mathf.FloorToInt((pointZ + z) / (this.chunkSize.z + 1));
					if(this.chunks.ContainsKey(coords))
						overlaping.Add(this.chunks[coords]);

					coords.x = Mathf.FloorToInt((pointX + x) / (this.chunkSize.x + 1));
					coords.y = Mathf.FloorToInt((pointZ + z) / (this.chunkSize.z));
					if(this.chunks.ContainsKey(coords))
						overlaping.Add(this.chunks[coords]);

				}
			}
		}
		return overlaping;
	}

#if UNITY_EDITOR
	private void OnValidate() {
		if(this.worldGenerator != null) {
			this.worldGenerator.SetBiomeTypes(biomeTypes);
		}
	}
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(InfiniteWorld))]
public class InfiniteWorldEditor : Editor {
    public override void OnInspectorGUI()
    {
        InfiniteWorld world = (InfiniteWorld)target;
        DrawDefaultInspector();

        if(GUILayout.Button("Generate")) {
            world.Generate();
        }
    }
}
#endif