using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

public struct ChunkData {
    public Vector3[] Vertices; 
    public int[] Triangles;
    public Color32[] Colors; 
    public float[] Noise;

    public ChunkData(Vector3[] vertices, int[] triangles, Color32[] colors, float[] noise) {
        this.Vertices = vertices;
        this.Triangles = triangles;
        this.Colors = colors;
        this.Noise = noise;
    }
}

public class Chunk : MonoBehaviour {
    // Private fields
    private Vector2Int coords;
    private Vector3 size;
    private Vector3 position;
    private int lod;

    // Public fields
    public ChunkData Data;
    public List<WorldResource> Resources;
    public List<WorldItem> Items;

    // Unity methods
    private void Awake() {
        this.Resources = new List<WorldResource>();
        this.Items = new List<WorldItem>();
    }

    // Public methods
    public void LowerTerrain(Vector3 point, int radius) {
        this.sculptSphere(point, radius, -1f);
    }

    public void HigherTerrain(Vector3 point, int radius) {
        this.sculptSphere(point, radius, 1f);
    }

    public void SetSize(Vector3 size) { this.size = size; }
    public void SetPosition(Vector3 position) { this.position = position; } 
    public void SetCoords(Vector2Int coords) { this.coords = coords; }
    public Vector3 GetSize() { return this.size; } 
    public Vector3 GetPosition() { return this.position; } 
    public Vector2Int GetCoords() { return this.coords; }

    // Private methods

    /// <summary>
    /// Changes noise data by value with spherical topography.
    /// </summary>
    /// <param name="point">Brush position</param>
    /// <param name="radius">Brush radius</param>
    /// <param name="value">Value by which data will be changed</param>
    private void sculptSphere(Vector3 point, int radius, float value) {
        for(int x = -radius; x < radius; ++x) {
            for(int y = 0; y < radius; ++y) {
                for(int z = -radius; z < radius; ++z) {
                    Vector3 vertPoint = new Vector3(Mathf.Round(point.x), Mathf.Round(point.y), Mathf.Round(point.z)) + new Vector3(x, y, z);

                    float maxX = this.position.x + this.size.x + 1;
                    float maxZ = this.position.z + this.size.z + 1;

                    //if(vertPoint.x > maxX || vertPoint.z > maxZ  || vertPoint.x < this.position.x - 1 || vertPoint.z < this.position.z - 1)
                    //    continue;

                    int noiseId = this.getNoiseId(vertPoint);
                    if(noiseId > this.Data.Noise.Length)
                        continue;
                    
                    float befValue = this.Data.Noise[noiseId];
                    this.Data.Noise[noiseId] = Mathf.Clamp01(this.Data.Noise[noiseId] + value); 
                }
            }
        }
    }
    
    /// <summary>
    /// Calculates index in noise array at given point
    /// </summary>
    /// <param name="point">World position</param>
    /// <returns>Array index</returns>
    private int getNoiseId(Vector3 point) {
        point.x = Mathf.Round(point.x);
        point.y = Mathf.Round(point.y);
        point.z = Mathf.Round(point.z);
        
        float x = point.x % this.size.x;
        float y = point.y % this.size.y;
        float z = point.z % this.size.z;

        if(point.x >= this.position.x + this.size.x)
            x += this.size.x;
        
        if(point.z >= this.position.z + this.size.z)
            z += this.size.z;

        int id = (int)( x + y * (size.x + 1) + z * (size.x + 1) * (size.y + 1) );
        return id;
    }
}