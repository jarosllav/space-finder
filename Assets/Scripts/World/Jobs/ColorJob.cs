using UnityEngine;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

[BurstCompile]
public struct ColorJob : IJob {
    // Input
    [ReadOnly] public NativeList<float3> Vertices;
    [ReadOnly] public NativeArray<int> BiomesIds;
    [ReadOnly] public NativeArray<float3> BiomesPositions;
    [ReadOnly] public NativeArray<int> BiomesColorsKeys;
    [ReadOnly] public NativeList<float4> BiomesColors;
    [ReadOnly] public float3 ChunkSize;
    [ReadOnly] public float3 Offset;
    [ReadOnly] public float Threshold;

    // Output
    public NativeArray<float3> Colors;

    public void Execute() { 
        for(int i = 0; i < Colors.Length; ++i) {
			float3 position = Vertices[i] + Offset;

			float firstDistance = float.MaxValue, secondDistance = float.MaxValue;
			int firstId = 0, secondId = 0;

			for(int j = 0; j < BiomesPositions.Length; ++j) {
				float dst = Mathf.Sqrt(Mathf.Pow(position.x - BiomesPositions[j].x, 2) + Mathf.Pow(position.z - BiomesPositions[j].z, 2));

				if(dst < firstDistance) {
					secondId = firstId;
					secondDistance = firstDistance;

					firstDistance = dst;
					firstId = j;
				}
			}

			float3 firstPosition = BiomesPositions[firstId], secondPosition = BiomesPositions[secondId];

            float d = 1f - firstDistance / secondDistance;
            float value = Vertices[i].y / (ChunkSize.y + 1);

            float3 baseColor = evaluate(value, BiomesIds[firstId]);

            if(Threshold != 0f && d < Threshold) {
				float f = 1f - ((d + Threshold) / (2f * Threshold));
                float3 lerpColor = evaluate(value, BiomesIds[secondId]);

                baseColor = math.lerp(baseColor, lerpColor, f);
            }

			Colors[i] = baseColor * 255;
		}
    }

    private float3 evaluate(float value, int biomeId) {
        int keysIndex = this.BiomesColorsKeys[biomeId * 2];
        int keysLength = this.BiomesColorsKeys[biomeId * 2 + 1];

        //value = math.clamp(0, 1, value);

        float4 color0 = new float4(255, 255, 255, 1);
        float4 color1 = new float4(255, 255, 255, 1);

        for(int i = 0; i < keysLength; ++i) {
            float4 colorKey = this.BiomesColors[keysIndex + i];

            float t = colorKey.w;
            if(t >= value) {
                int index = keysIndex + i - 1;
                if(index < 0)
                    index = 0;

                color0 = this.BiomesColors[index];
                color1 = colorKey;
                break;
            }
        }

        float ratio = (value - color0.w) / (color1.w - color0.w);

        float w = ratio * 2f - 1f;
        float w0 = (w / 1f + 1f) / 2f;
        float w1 = 1f - w0;

        float3 color = new float3(
            color0.x * w0 + color1.x * w1, 
            color0.y * w0 + color1.y * w1, 
            color0.z * w0 + color1.z * w1
        );

        return color;
    }
}