using UnityEngine;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

[BurstCompile]
public struct NoiseJob : IJob {
    // Input
    [ReadOnly] public NativeArray<float> Scale;
    [ReadOnly] public NativeArray<float> Lacunarity;
    [ReadOnly] public NativeArray<float> Persistence;
    [ReadOnly] public NativeArray<int> Octaves;
    [ReadOnly] public float InterpolateThreshold;
    [ReadOnly] public float GradientThreshold;
    [ReadOnly] public float3 Offset;
    [ReadOnly] public float3 Size;
    [ReadOnly] public int Seed;
    [ReadOnly] public NativeArray<float3> BiomesPositions;
    [ReadOnly] public NativeArray<int> BiomesTypes;

    // Output
    public NativeArray<float> Noise;

    public void Execute() {
        int width = (int)Size.x + 1;
        int height = (int)Size.y + 1;
        int depth = (int)Size.z + 1;

        int biomesLength = Scale.Length;

        for(int x = 0; x < width; ++x) {
           for(int y = 0; y < height; ++y) {
            	for(int z = 0; z < depth; ++z) {
                    // Position of vertex in world space
                    float3 position = Offset + new float3(x, y, z);

                    // Look for two closest BiomesPositions
                    float firstDistance = float.MaxValue, secondDistance = float.MaxValue;
                    int firstId = 0, secondId = 0;

                    for(int i = 0; i < BiomesPositions.Length; ++i) {
                        float3 centroid = BiomesPositions[i];
                        float dst = math.sqrt(math.pow(position.x - centroid.x, 2) + math.pow(position.z - centroid.z, 2));

                        if(dst < firstDistance) {
                            secondId = firstId;
                            secondDistance = firstDistance;

                            firstDistance = dst;
                            firstId = i;
                        }
                    }

                    float3 firstPosition = BiomesPositions[firstId];
                    float3 secondPosition = BiomesPositions[secondId];
                    int firstType = BiomesTypes[firstId];
                    int secondType = BiomesTypes[secondId];

                    // Calculate falloff
                    float d = 1f - firstDistance / secondDistance;

                    // Setup closest biome noise settings
                    float persistence = Persistence[firstType];
                    float lacunarity = Lacunarity[firstType];
                    float scale = Scale[firstType];
                    int octaves = Octaves[firstType];

                    // Get perlin noise value from base biome
                    float value = getNoiseValue(x, y, z, persistence, lacunarity, scale, octaves);

                    if(InterpolateThreshold != 0f && d < InterpolateThreshold) {
                        persistence = Persistence[secondType];
                        lacunarity = Lacunarity[secondType];
                        scale = Scale[secondType];
                        octaves = Octaves[secondType];

                        float f = 1f - ((d + InterpolateThreshold) / (2f * InterpolateThreshold));
                        float secondValue = getNoiseValue(x, y, z, persistence, lacunarity, scale, octaves);

                        value = math.lerp(value, secondValue, f);
                    }

                    int id = x + y * width + z * width * height;
                    Noise[id] = value;
                }
            }
        }
    }

    private float getNoiseValue(int x, int y, int z, float persistence, float lacunarity, float scale, int octaves) {
        // Sample octave perlin noise value
        float value = 0f;
        float frequency = 1f;
        float amplitude = 1f;
        for(int octave = 0; octave < octaves; ++octave) {
            float3 sample = (new float3(x, y, z) + Offset) / scale * frequency + Seed;
            float perlin = noise.cnoise(sample);
            value += perlin * amplitude;

            amplitude *= persistence;
            frequency *= lacunarity;
        }

        // Clamp value between 0 - 1
        value = (math.clamp(value, -1, 1) + 1f) / 2f;

        // Linear gradient
        float step = (float)y / ((int)Size.y + 1);
        float gradientValue = math.clamp(value / (1f - GradientThreshold), 0f, 1f);
        
        value *= gradientValue;
        value *= ((int)Size.y + 1) * (1f - GradientThreshold);

        if(y <= value - 0.5f)
            value = 0f;
        else if(y > value + 0.5f)
            value = 1f;
        else if(y > value)
            value = (float)y - value;
        else
            value = value - (float)y;

        return 1f - math.clamp(value, 0, 1);
    }
}