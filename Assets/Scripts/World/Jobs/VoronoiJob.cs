using UnityEngine;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

[BurstCompile]
public struct VoronoiJob : IJob {
    // Input
    [ReadOnly] public float2 Size;
    [ReadOnly] public float2 Offset;
    [ReadOnly] public uint Seed;
    [ReadOnly] public int RegionsCount;

    // Output
    public NativeArray<Vector3> Points;

    public void Execute() { 
        Unity.Mathematics.Random random = new Unity.Mathematics.Random(Seed);

        for(int i = 0; i < RegionsCount; ++i) {
            Points[i] = new Vector3(Offset.x + random.NextFloat(0, Size.x), 0, Offset.y + random.NextFloat(0, Size.y));
        }
    }
}