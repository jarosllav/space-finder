using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

public class ChunkJob {
	// Main chunk data
	public NativeList<float3> Vertices;
	public NativeList<int> Triangles;
	public NativeArray<float3> Colors;
	public NativeArray<float> Noise;

	// Shared data
	public NativeArray<int> BiomesTypes;
	public NativeArray<float3> BiomesPositions;
	public NativeList<float4> BiomesColors;
	public NativeArray<int> BiomesColorsKeys;
	public float3 ChunkPosition;
	public float3 ChunkSize;
	public float InterpolateThreshold;

	// Noise data 
	public int Seed;
	public NativeArray<float> Scales;
	public NativeArray<float> Lacunarities;
	public NativeArray<float> Persistances;
	public NativeArray<int> Octaves;
	public float GradientThreshold;

	// Marching cubes data
	public int LevelOfDetail;
	public float SurfaceLevel;

	// Public fields
	public JobHandle Handle;
	public Action Callback;

	// Private fields
	private JobHandle noiseHandle;
	private JobHandle marchingHandle;
	private JobHandle colorHandle;
	private bool isNoiseScheduled = false; 

	public ChunkJob(BiomeSettings[] biomesTypes, List<Biome> neighbourBiomes, float3 chunkPosition, float3 chunkSize, float threshold) {
		this.ChunkPosition = chunkPosition;
		this.ChunkSize = chunkSize;
		this.InterpolateThreshold = threshold;

		this.Vertices = new NativeList<float3>(Allocator.Persistent);
		this.Triangles = new NativeList<int>(Allocator.Persistent);
		this.Noise = new NativeArray<float>((int)((this.ChunkSize.x + 1) * (this.ChunkSize.y + 1) * (this.ChunkSize.z + 1)), Allocator.Persistent);

		this.BiomesTypes = new NativeArray<int>(neighbourBiomes.Count, Allocator.Persistent);
		this.BiomesPositions = new NativeArray<float3>(neighbourBiomes.Count, Allocator.Persistent);

		this.BiomesColorsKeys = new NativeArray<int>(biomesTypes.Length * 2, Allocator.Persistent);
		this.BiomesColors = new NativeList<float4>(Allocator.Persistent);

		for(int i = 0; i < neighbourBiomes.Count; ++i) {
			this.BiomesTypes[i] = neighbourBiomes[i].BiomeSettingsId;
			this.BiomesPositions[i] = neighbourBiomes[i].Position;
		}

		for(int i = 0; i < biomesTypes.Length; ++i) {
			GradientColorKey[] keys = biomesTypes[i].Color.colorKeys;

			this.BiomesColorsKeys[i * 2] = this.BiomesColors.Length;	// Position in array
			this.BiomesColorsKeys[i * 2 + 1] = keys.Length;				// Length of keys

			for(int j = 0; j < keys.Length; ++j) {
				Color color = keys[j].color;
				float time = keys[j].time;
				this.BiomesColors.Add(new float4(color.r, color.g, color.b, time));
			}
		}

	}

	// Public methods
	public void Schedule() {
		this.noiseHandle = this.scheduleNoise();
		this.schedule();	
	}

	public void ScheduleWithNoise(float[] noise) {
		this.Noise.CopyFrom(noise);
		this.isNoiseScheduled = true;
		this.schedule();	
	}

	public void Complete() {
		//Handle.Complete();
	}

	public void Dispose() {
		this.Vertices.Dispose();
		this.Triangles.Dispose();
		this.BiomesTypes.Dispose();
		this.BiomesPositions.Dispose();
		this.BiomesColors.Dispose();
		this.BiomesColorsKeys.Dispose();
		this.Noise.Dispose();
		this.Colors.Dispose();
	}

	public Vector3[] GetVertices() {
		Vector3[] vertices = new Vector3[this.Vertices.Length];
		for(int i = 0; i < vertices.Length; ++i) {
			float3 vertex = this.Vertices[i];
			vertices[i] = vertex;
		}
		return vertices;
	}

	public int[] GetTriangles() {
		return this.Triangles.ToArray();
	}

	public float[] GetNoise() {
		return this.Noise.ToArray();
	}

	public Color32[] GetColors() {
		Color32[] colors = new Color32[this.Colors.Length];
		for(int i = 0; i < colors.Length; ++i) {
			float3 color = this.Colors[i];
			colors[i] = new Color32((byte)color.x, (byte)color.y, (byte)color.z, 255);
		}
		return colors;
	}

	// Private methods
	private void schedule() {
		this.marchingHandle = this.scheduleMarching();
		this.marchingHandle.Complete();

		this.colorHandle = this.scheduleColors();
		this.colorHandle.Complete();

		this.Callback?.Invoke();
		this.isNoiseScheduled = false;
	}

	private JobHandle scheduleNoise() {
		NoiseJob noiseJob = new NoiseJob();
		noiseJob.Seed = this.Seed;
		noiseJob.Scale = this.Scales;
        noiseJob.Lacunarity = this.Lacunarities;
        noiseJob.Persistence = this.Persistances;
        noiseJob.Octaves = this.Octaves;
		noiseJob.InterpolateThreshold = this.InterpolateThreshold;
		noiseJob.GradientThreshold = this.GradientThreshold;
		noiseJob.Offset = this.ChunkPosition;
		noiseJob.Size = this.ChunkSize;
		noiseJob.BiomesTypes = this.BiomesTypes;
		noiseJob.BiomesPositions = this.BiomesPositions;

		// Result
        noiseJob.Noise = this.Noise;

		return noiseJob.Schedule();
	}

	private JobHandle scheduleMarching() {
		MarchingJob marchingJob = new MarchingJob();
        marchingJob.LOD = this.LevelOfDetail;
        marchingJob.Size = this.ChunkSize;
        marchingJob.Noise = this.Noise;
        marchingJob.SurfaceLevel = this.SurfaceLevel;

		// Result
        marchingJob.Vertices = this.Vertices;
        marchingJob.Indices = this.Triangles;

		if(!this.isNoiseScheduled)
			return marchingJob.Schedule(this.noiseHandle);
		else
			return marchingJob.Schedule();
	}

	private JobHandle scheduleColors() {
		this.Colors = new NativeArray<float3>(this.Vertices.Length, Allocator.Persistent);
		
		ColorJob colorJob = new ColorJob();
		colorJob.Vertices = this.Vertices;
		colorJob.BiomesIds = this.BiomesTypes;
		colorJob.BiomesPositions = this.BiomesPositions;
		colorJob.BiomesColorsKeys = this.BiomesColorsKeys;
		colorJob.BiomesColors = this.BiomesColors;
		colorJob.ChunkSize = this.ChunkSize;
		colorJob.Offset = this.ChunkPosition;
		colorJob.Threshold = this.InterpolateThreshold;

		// Result
		colorJob.Colors = this.Colors;

		return colorJob.Schedule();
	}
}

public struct ChunkJobData {
	public JobHandle Handle;
	public System.Action<Chunk> Callback;

	// Shared data
	public NativeArray<float3> Biomes;
	public NativeArray<int> BiomesIds;
	// NativeArray<NativeArray<float4>>BiomesColors

	public Vector3 Position;
	public Vector3 ChunkSize;
	public Vector2Int Coords;
	public float Threshold;
	public int Lod;

	// Output
	public NativeArray<float> Noise;
	public NativeArray<float3> Colors;
	public NativeList<Vector3> Vertices;
	public NativeList<int> Triangles;

	public void Dispose() {
		this.Vertices.Dispose();
		this.Triangles.Dispose();
		this.Noise.Dispose();

		this.Biomes.Dispose();
		this.BiomesIds.Dispose();
	}
}

/*[BurstCompile]
public struct ChunkJob : IJob {
    // Input
    [ReadOnly] public float3 size;
    [ReadOnly] public int2 coords;
	[ReadOnly] public int lod;

    // Output
    public NativeList<Vector3> vertexList;
   	public NativeList<int> trianglesList;

    public void Execute() {
		float3 offset = new float3(size.x * coords.x, 0, size.z * coords.y);

        int width = (int)size.x + 1;
        int height = (int)size.y + 1;
        int depth = (int)size.z + 1;

        NativeArray<float> noiseArray = new NativeArray<float>(width * height * depth, Allocator.Temp);

        // Noise settings
        float noiseScale = 15f;
        float noiseLacunarity = 0.3f;
        float noisePersistence = 0.75f;
        int noiseOctaves = 4;

        float gradientThreshold = 0.3f;

        // Fill noise
        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
            	for(int z = 0; z < depth; ++z) {
                    // Evaulate noise
                    float noiseValue = 0f;
                    float frequency = 1f;
                    float amplitude = 1f;
                    for(int octave = 0; octave < noiseOctaves; ++octave) {
						float3 sample = (new float3(x, y, z) + offset) / noiseScale * frequency;
                        float perlin = noise.cnoise(sample);
                        noiseValue += perlin * amplitude;
                        amplitude *= noisePersistence;
                        frequency *= noiseLacunarity;
                    }
                    noiseValue = (math.clamp(noiseValue, -1, 1) + 1f) / 2f;

                    // Linear gradient
                    float step = (float)y / height;
                    float gradientValue = math.clamp(noiseValue / (1f - gradientThreshold), 0f, 1f);
					
                    //noiseValue += 0.3f;
                    noiseValue *= gradientValue;
					noiseValue += gradientThreshold;

					noiseValue *= height * (1f - gradientThreshold);
					if(y <= noiseValue - 0.5f)
						noiseValue = 0f;
					else if(y > noiseValue + 0.5f)
						noiseValue = 1f;
					else if(y > noiseValue)
						noiseValue = (float)y - noiseValue;
					else
						noiseValue = noiseValue - (float)y;

					noiseValue = 1f - noiseValue;

                    int id = x + y * width + z * width * height;
                    noiseArray[id] = noiseValue;
                }
            }
        }

        // Marching cubes algorithm
		int lodIncrement = lod == 0 ? 1 : lod * 2;

        float surfaceLevel = 0.5f;
        for(int x = 0; x < width - 1; x += lodIncrement) {
            for(int y = 0; y < height - 1; y += lodIncrement) {
                for(int z = 0; z < depth - 1; z += lodIncrement) {
                    float3 position = new float3(x, y, z);
                    int configuration = 0;

                    for(int i = 0; i < 8; ++i) {
                        int3 corner = new int3(x, y, z) + Corners[i] * lodIncrement;
                        int cornerId = corner.x + corner.y * width + corner.z * width * height;

                        if(noiseArray[cornerId] < surfaceLevel) {
                            configuration |= 1 << i;
                        }
                    }

                    // March cube
                    march(new float3(x, y, z), configuration, lodIncrement);
                }
            }
        }

    }

    public void march(float3 position, int configuration, int lodIncrement) {
        if(configuration == 0 || configuration == 255)
            return;
        
        int edgeIndex = 0;
        for(int i = 0; i < 5; ++i) {
            for(int p = 0; p < 3; ++p) {
                int triangleIndex = Triangles[16 * configuration + edgeIndex];
                if(triangleIndex == -1)
                    return;
                
                float3 vertex1 = position + Edges[triangleIndex * 2] * lodIncrement;
                float3 vertex2 = position + Edges[triangleIndex * 2 + 1] * lodIncrement;
                float3 vertex = (vertex1 + vertex2) / 2f;

                vertexList.Add(vertex);
                trianglesList.Add(vertexList.Length - 1);

                edgeIndex++;
            }
        }
    }
	
    readonly public static int3[] Corners = new int3[8] {
        new int3(0, 0, 0),
        new int3(1, 0, 0),
        new int3(1, 1, 0),
        new int3(0, 1, 0),
        new int3(0, 0, 1),
        new int3(1, 0, 1),
        new int3(1, 1, 1),
        new int3(0, 1, 1)
    };
    readonly public static float3[] Edges = new float3[24] {
        new float3(0.0f, 0.0f, 0.0f), new float3(1.0f, 0.0f, 0.0f),
        new float3(1.0f, 0.0f, 0.0f), new float3(1.0f, 1.0f, 0.0f),
        new float3(0.0f, 1.0f, 0.0f), new float3(1.0f, 1.0f, 0.0f),
        new float3(0.0f, 0.0f, 0.0f), new float3(0.0f, 1.0f, 0.0f),
        new float3(0.0f, 0.0f, 1.0f), new float3(1.0f, 0.0f, 1.0f),
        new float3(1.0f, 0.0f, 1.0f), new float3(1.0f, 1.0f, 1.0f),
        new float3(0.0f, 1.0f, 1.0f), new float3(1.0f, 1.0f, 1.0f),
        new float3(0.0f, 0.0f, 1.0f), new float3(0.0f, 1.0f, 1.0f),
        new float3(0.0f, 0.0f, 0.0f), new float3(0.0f, 0.0f, 1.0f),
        new float3(1.0f, 0.0f, 0.0f), new float3(1.0f, 0.0f, 1.0f),
    	new float3(1.0f, 1.0f, 0.0f), new float3(1.0f, 1.0f, 1.0f),
        new float3(0.0f, 1.0f, 0.0f), new float3(0.0f, 1.0f, 1.0f)
    };
    readonly public static int[] Triangles = new int[16 * 256] {
	    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1,
	    3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1,
	    3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1,
	    3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1,
	    9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1,
	    1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1,
	    9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1,
	    2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1,
	    8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1,
	    9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1,
	    4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1,
	    3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1,
	    1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1,
	    4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1,
	    4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1,
	    9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1,
	    1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1,
	    5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1,
	    2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1,
	    9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1,
	    0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1,
	    2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1,
	    10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1,
	    4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1,
	    5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1,
	    5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1,
	    9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1,
	    0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1,
	    1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1,
	    10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1,
	    8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1,
	    2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1,
	    7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1,
	    9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1,
	    2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1,
	    11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1,
	    9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1,
	    5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1,
	    11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1,
	    11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1,
	    1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1,
	    9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1,
	    5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1,
	    2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1,
	    0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1,
	    5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1,
	    6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1,
	    0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1,
	    3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1,
	    6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1,
	    5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1,
	    1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1,
	    10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1,
	    6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1,
	    1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1,
	    8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1,
	    7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1,
	    3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1,
	    5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1,
	    0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1,
	    9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1,
	    8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1,
	    5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1,
	    0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1,
	    6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1,
	    10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1,
	    10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1,
	    8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1,
	    1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1,
	    3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1,
	    0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1,
	    10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1,
	    0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1,
	    3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1,
	    6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1,
	    9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1,
	    8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1,
	    3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1,
	    6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1,
	    0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1,
	    10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1,
	    10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1,
	    1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1,
	    2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1,
	    7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1,
	    7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1,
	    2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1,
	    1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1,
	    11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1,
	    8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1,
	    0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1,
	    7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1,
	    10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1,
	    2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1,
	    6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1,
	    7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1,
	    2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1,
	    1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1,
	    10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1,
	    10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1,
	    0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1,
	    7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1,
	    6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1,
	    8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1,
	    9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1,
	    6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1,
	    1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1,
	    4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1,
	    10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1,
	    8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1,
	    0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1,
	    1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1,
	    8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1,
	    10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1,
	    4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1,
	    10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1,
	    5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1,
	    11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1,
	    9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1,
	    6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1,
	    7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1,
	    3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1,
	    7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1,
	    9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1,
	    3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1,
	    6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1,
	    9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1,
	    1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1,
	    4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1,
	    7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1,
	    6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1,
	    3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1,
	    0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1,
	    6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1,
	    1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1,
	    0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1,
	    11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1,
	    6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1,
	    5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1,
	    9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1,
	    1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1,
	    1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1,
	    10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1,
	    0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1,
	    5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1,
	    10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1,
	    11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1,
	    0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1,
	    9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1,
	    7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1,
	    2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1,
	    8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1,
	    9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1,
	    9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1,
	    1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1,
	    9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1,
	    9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1,
	    5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1,
	    0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1,
	    10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1,
	    2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1,
	    0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1,
	    0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1,
	    9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1,
	    5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1,
	    3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1,
	    5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1,
	    8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1,
	    0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1,
	    9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1,
	    0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1,
	    1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1,
	    3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1,
	    4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1,
	    9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1,
	    11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1,
	    11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1,
	    2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1,
	    9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1,
	    3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1,
	    1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1,
	    4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1,
	    4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1,
	    0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1,
	    3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1,
	    3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1,
	    0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1,
	    9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1,
	    1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    };
}*/