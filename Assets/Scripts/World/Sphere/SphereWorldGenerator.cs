using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

public class SphereWorldGenerator
{
    // Public fields
    public BiomeSettings[] Biomes;
    public float[,,] Noise;
    public Color[,,] Colors;

    // Private fields
    private NoiseGenerator noiseGenerator = new NoiseGenerator();
    private VoronoiSphere voronoi = new VoronoiSphere();

    public SphereWorldGenerator() {
        // ...
    }

    // Public methods

    /// <summary>
    /// Generates noise values and colors for every possible vertex
    /// </summary>
    /// <param name="radius">Sphere radius</param>
    public void GenerateSphere(int radius) {
        this.Noise = new float[radius * 2 + 1, radius * 2 + 1, radius * 2 + 1];
        this.Colors = new Color[radius * 2 + 1, radius * 2 + 1, radius * 2 + 1];

        this.noiseGenerator.Randomize();
        this.voronoi.Generate(25, this.Biomes.Length, (float)radius);

        Vector3 center = Vector3.one * radius;
        float maxDist = Vector2.Distance(new Vector2(center.x, center.z), new Vector2(radius * 2, radius * 2)); 

        for(int x = 0; x < this.Noise.GetLength(0); ++x) {
            for(int y = 0; y < this.Noise.GetLength(1); ++y) {
                int threshold = y > radius ? radius * 2 - y : y;
                for(int z = 0; z < this.Noise.GetLength(2); ++z) {
                    var centroid = this.voronoi.GetClosestCentroid(new Vector3(x, y, z));
                    NoiseSettings noiseSettings = this.Biomes[centroid.Item2].NoiseSettings;

                    if(noiseSettings) {
                        this.noiseGenerator.Scale = noiseSettings.Scale;
                        this.noiseGenerator.Octaves = noiseSettings.Octaves;
                        this.noiseGenerator.Lacunarity = noiseSettings.Lacunarity;
                        this.noiseGenerator.Persistence = noiseSettings.Persistence;

                        float noiseValue = this.noiseGenerator.Evaluate(new Vector3(x, y, z));
                        noiseValue = (noiseValue + 1f) / 2f;

                        float distFromCenter = Vector2.Distance(center, new Vector2(x, z));
                        float maskPixel = (0.5f - (distFromCenter / (radius * 2))) * threshold;
                        noiseValue *= maskPixel;

                        Noise[x, y, z] = noiseValue;

                        Color color = this.Biomes[centroid.Item2].Color.Evaluate(Mathf.Clamp01(distFromCenter / maxDist));
                        Colors[x, y, z] = color;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Randomize world generation seed
    /// </summary>
    public void RandomizeSeed() {
        this.noiseGenerator.Randomize();
    }
}
