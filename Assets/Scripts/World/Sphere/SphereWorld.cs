using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter)), RequireComponent(typeof(MeshRenderer))]
public class SphereWorld : MonoBehaviour, IWorld
{
    // Public fields
    public BiomeSettings[] Biomes;
    public int Radius = 15;

    // Private fields
    private MarchingCubes marchingCubes = new MarchingCubes();
    private SphereGenerator sphereGenerator = new SphereGenerator();
    private SphereWorldGenerator worldGenerator = new SphereWorldGenerator();
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    // Unity methods
    private void Awake() {
        this.meshFilter = gameObject.GetComponent<MeshFilter>();
        this.meshRenderer = gameObject.GetComponent<MeshRenderer>();
    }

    // Public methods

    /// <summary>
    /// Generates spherical world
    /// </summary>
    public void Generate() {
        this.worldGenerator.Biomes = this.Biomes;
        this.worldGenerator.GenerateSphere(this.Radius);

        Color[,,] colors = this.worldGenerator.Colors;
        float[,,] noise = this.worldGenerator.Noise;

        float diameter = this.Radius * 2;
        Mesh mesh = marchingCubes.Generate(noise, colors, new Vector3(diameter, diameter, diameter));
        mesh.name = "Sphere world";

        transform.position = new Vector3(-this.Radius, -this.Radius, 0);

        meshFilter.mesh = mesh;
    }

    // Private methods
    // ...    
}

#if UNITY_EDITOR
[CustomEditor(typeof(SphereWorld))]
public class SphereWorldEditor : Editor {
    public override void OnInspectorGUI()
    {
        SphereWorld world = (SphereWorld)target;
        DrawDefaultInspector();

        if(GUILayout.Button("Generate")) {
            world.Generate();
        }
    }
}
#endif