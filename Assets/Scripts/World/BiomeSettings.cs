﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Biome", order = 1)]
public class BiomeSettings : ScriptableObject
{
    public string Name;
    public Gradient Color;
    public float Chance;
    public NoiseSettings NoiseSettings;

    [Header("Resources")]
    public GameObject[] Resources;
}