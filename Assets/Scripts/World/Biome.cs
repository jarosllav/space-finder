using UnityEngine;

public class Biome {
    public int Id;
    public BiomeSettings BiomeSettings;
    public int BiomeSettingsId;
    public Vector3 Position;
}