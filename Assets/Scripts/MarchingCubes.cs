﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MarchingCubes
{
    // Public fields
    public float SurfaceLevel = 0.5f;

    // Private fields
    private List<Vector3> vertices = new List<Vector3>();
    private List<int> triangles = new List<int>();
    private List<Color32> colors32 = new List<Color32>();

    private float[,,] noise;
    private Color[,,] colors;

    public MarchingCubes() {
        // ...
    }

    // Public methods

    /// <summary>
    /// Generates mesh using marching-cubes algorithm
    /// </summary>
    /// <param name="noise">Noise values for every possible vertex</param>
    /// <param name="colors">Color value for every possible vertex</param>
    /// <param name="size">Mesh size (TODO: delete this and use length of noise array)</param>
    /// <returns></returns>
    public Mesh Generate(float[,,] noise, Color[,,] colors, Vector3 size) {
        this.vertices.Clear();
        this.triangles.Clear();
        this.colors32.Clear();
        
        this.noise = noise;
        this.colors = colors;

        for(int x = 0; x < size.x; ++x) {
            for(int y = 0; y < size.y; ++y) {
                for(int z = 0; z < size.z; ++z) {
                    Vector3 position = new Vector3(x, y, z);
                    float[] cube = new float[8];
                    for(int i = 0; i < 8; i++) {
                        Vector3Int corner = new Vector3Int(x, y, z) + MarchingStatic.Corners[i];
                        cube[i] = this.noise[corner.x, corner.y, corner.z];
                    }

                    march(new Vector3(x, y, z), this.getConfigurationIndex(cube));
                }
            }
        }

        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.colors32 = colors32.ToArray();
        mesh.RecalculateNormals();

        return mesh;
    }

    // Private methods

    /// <summary>
    /// Adds vertices, triangles and color to lists for cube at given position
    /// </summary>
    /// <param name="position">Position of cube</param>
    /// <param name="configuration">Configuration index (indicating to specific MarchingStatic.Triangles combination)</param>
    private void march(Vector3 position, int configuration) {
        if(configuration == 0 || configuration == 255) 
            return;
        
        int edgeIndex = 0;
        for(int i = 0; i < 5; i++) {
            for(int p = 0; p < 3; p++) {
                int triangleIndex = MarchingStatic.Triangles[configuration, edgeIndex];
                if(triangleIndex == -1)
                    return;
                
                Vector3 vertex1 = position + MarchingStatic.Edges[triangleIndex, 0];
                float noise1 = noise[(int)vertex1.x, (int)vertex1.y, (int)vertex1.z];
                Vector3 vertex2 = position + MarchingStatic.Edges[triangleIndex, 1];
                float noise2 = noise[(int)vertex2.x, (int)vertex2.y, (int)vertex2.z];

                Vector3 vertex = (vertex1 + vertex2) / 2f;
                //float mu = (SurfaceLevel - noise1) / (noise2 - noise1);
                //Vector3 vertex = vertex1 + mu * (vertex2 - vertex1);
                Color color = colors[(int)vertex.x, (int)vertex.y, (int)vertex.z];

                vertices.Add(vertex);
                triangles.Add(vertices.Count - 1);
                colors32.Add(color);

                edgeIndex++;
            }
        }
    }
    
    private int getConfigurationIndex(float[] values) {
        int index = 0;
        for(int i = 0; i < 8; ++i) {
            if(values[i] < SurfaceLevel) {
                index |= 1 << i;
            }
        }
        return index;
    }

}
