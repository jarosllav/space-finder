﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereGenerator
{
    // Private fields
    private int gridSize;
    private float radius = 5f;
    private Mesh mesh;
    private List<Vector3> vertices;
    private List<Vector3> normals;
    private List<Color32> uvs;
    private List<int> indices;
    
    public SphereGenerator() {
        this.vertices = new List<Vector3>();
        this.uvs = new List<Color32>();
        this.indices = new List<int>();
    }

    // Private methods

    /// <summary>
    /// Generates all faces of the sphere
    /// </summary>
    private void createSphere() {
        this.vertices.Clear();
        this.indices.Clear();
        this.uvs.Clear();

        this.createFace(Vector3.up);        
        this.createFace(Vector3.down);        
        this.createFace(Vector3.left);        
        this.createFace(Vector3.right);        
        this.createFace(Vector3.forward);        
        this.createFace(Vector3.back);      
    }

    /// <summary>
    /// Generate sphere face of given direction 
    /// </summary>
    /// <param name="direction">Direction of the face</param>
    private void createFace(Vector3 direction) {
        Vector3 axisX = new Vector3(direction.y, direction.z, direction.x);
        Vector3 axisY = Vector3.Cross(direction, axisX);

        int verticesCount = this.vertices.Count;

        for(int y = 0; y < this.gridSize; ++y) {
            for(int x = 0; x < this.gridSize; ++x) {
                Vector2 percent = new Vector2(x, y) / (this.gridSize - 1);
                Vector3 unitCube = direction + ( (percent.x - 0.5f) * axisX + (percent.y - 0.5f) * axisY ) * 2;
                Vector3 unitSphere = unitCube;

                float powX = unitSphere.x * unitSphere.x;
                float powY = unitSphere.y * unitSphere.y;
                float powZ = unitSphere.z * unitSphere.z;

                unitSphere.x = unitSphere.x * Mathf.Sqrt(1f - powY / 2f - powZ / 2f + powY * powZ / 3f);
                unitSphere.y = unitSphere.y * Mathf.Sqrt(1f - powX / 2f - powZ / 2f + powX * powZ / 3f);
                unitSphere.z = unitSphere.z * Mathf.Sqrt(1f - powX / 2f - powY / 2f + powX * powY / 3f);

                this.vertices.Add(unitSphere * radius);

                int vertexId = x + y * gridSize + verticesCount;
                if(x != gridSize - 1 && y != gridSize - 1) {
                    this.indices.Add(vertexId);
                    this.indices.Add(vertexId + gridSize + 1);
                    this.indices.Add(vertexId + gridSize);
                    this.indices.Add(vertexId);
                    this.indices.Add(vertexId + 1);
                    this.indices.Add(vertexId + gridSize + 1);
                }
            }
        }
    }

    // Public methods

    /// <summary>
    /// Generate sphere mesh
    /// </summary>
    /// <param name="size">Sphere size</param>
    /// <returns>Mesh instance</returns>
    public Mesh GenerateMesh(int size) {
        this.gridSize = size;
        if(this.mesh == null) {
            this.mesh = new Mesh();
        }
        else {
            this.mesh.Clear();
        }
        this.mesh.name = "Sphere";

        this.createSphere();

        this.mesh.vertices = this.vertices.ToArray();
        this.mesh.triangles = this.indices.ToArray();
        return this.mesh;
    }

    public List<Vector3> GetVertices() { return vertices; }

    public void SetGridSize(int size) { gridSize = size; }

}
