﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    // Private fields
    [SerializeField] private Transform target;
    private Camera camera;
 
    private Vector2 boundsVertical = new Vector2(-40f, 90f);
    private float sensivity = 0.25f;
    private float distance = 4f;

    private float rotationX;
    private float rotationY;
    private int layerMask;

    // Unity methods
    private void Awake() {
        this.layerMask = 1 << LayerMask.NameToLayer("Ignore Player Collision");
        this.layerMask =~ this.layerMask;
        this.camera = Camera.main;
    }

    private void Update() {
        if(this.target != null) {
            transform.position = this.target.position;
            this.updateRotation();
            this.updateDistance();
        }
    }

    // Private methods

    /// <summary>
    /// Rotating the camera around a target by mouse movement.
    /// TODO: Rotating bounds
    /// </summary>
    private void updateRotation() {
        float rotY = Input.GetAxis("Mouse X") * 180f * this.sensivity * Time.deltaTime;
        float rotX = Input.GetAxis("Mouse Y") * 180f * this.sensivity * Time.deltaTime;

        this.rotationX = Mathf.Clamp(rotationX += rotX, -40f, 40f);
        this.rotationY = rotationY + rotY;
        transform.localRotation = Quaternion.Euler(rotationX, rotationY, 0f);
    }

    /// <summary>
    /// Positions the camera behind a target, trim distance if encounter an obstacle.
    /// </summary>
    private void updateDistance() {
        RaycastHit hit;
        if(Physics.Raycast(this.target.position, -transform.forward, out hit, this.distance, this.layerMask)) 
            transform.position = hit.point;
        else 
            transform.Translate(new Vector3(0, 0, -this.distance));
    }

    // Public methods
    public void SetDistance(float distance)     { this.distance = Mathf.Clamp(distance, 1f, 10f); }
    public void SetSensivity(float sensivity)   { this.sensivity = sensivity; }
    public float GetDistance()  { return this.distance; }
    public float GetSensivity() { return this.sensivity; }
}
