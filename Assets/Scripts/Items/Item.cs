using UnityEngine;
using UnityEditor;

public enum ItemType {
    NORMAL,
    PICKAXE,
    SHOVEL,
    SWORD
}

[CreateAssetMenu(fileName = "Item", menuName = "Item")]
public class Item : ScriptableObject {
    public ItemType Type;
    public string Name;
    public string Code;
    public string Description;
    public float Weight = 1f;
    public int MaxStack = 60;
    
    public Sprite Image;
    public Mesh Model;
    public Material[] Materials;
    
    public bool IsConsumable = false;
    public bool DestoryAtConsume = true;
}

#if UNITY_EDITOR
[CustomEditor(typeof(Item))]
public class ItemEditor : Editor {
    private static bool foldBasic;
    private static bool foldVisual;

    public override void OnInspectorGUI() {
        Item item = (Item)target;

        EditorGUI.BeginChangeCheck();

        foldBasic = EditorGUILayout.Foldout(foldBasic, "Basic information");
        
        if(foldBasic) {
            item.Type = (ItemType)EditorGUILayout.EnumPopup("Type", item.Type);
            item.Name = EditorGUILayout.TextField("Name", item.Name);
            item.Code = EditorGUILayout.TextField("Code", item.Code);
            item.Description = EditorGUILayout.TextField("Description", item.Description);
            item.Weight = EditorGUILayout.FloatField("Weight", item.Weight);
            item.MaxStack = EditorGUILayout.IntField("Max stack", item.MaxStack);
        }

        foldVisual = EditorGUILayout.Foldout(foldVisual, "Visual information");

        if(foldVisual) {
            item.Image = (Sprite)EditorGUILayout.ObjectField("Image", item.Image, typeof(Sprite), allowSceneObjects:true);
            item.Model = (Mesh)EditorGUILayout.ObjectField("Model", item.Model, typeof(Mesh), allowSceneObjects:true);

            SerializedObject serializedObject = new SerializedObject(target);
            SerializedProperty property = serializedObject.FindProperty("Materials");
            serializedObject.Update();
            EditorGUILayout.PropertyField(property, true);
            serializedObject.ApplyModifiedProperties();
        }

        EditorGUILayout.Space(10);

        EditorGUILayout.LabelField("Consumable information", EditorStyles.boldLabel);
        item.IsConsumable = EditorGUILayout.Toggle("Is consumable", item.IsConsumable);
        if(item.IsConsumable) {
            item.DestoryAtConsume = EditorGUILayout.Toggle("Destroy at consume", item.DestoryAtConsume);
        }

        EditorGUI.EndChangeCheck();
    }
}
#endif