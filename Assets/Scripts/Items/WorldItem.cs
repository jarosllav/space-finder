using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshCollider)), RequireComponent(typeof(MeshFilter)), 
 RequireComponent(typeof(MeshRenderer)), RequireComponent(typeof(Rigidbody))]
public class WorldItem : MonoBehaviour{
    // Public fields
    public Item Data;
    public int Amount;
    public bool IsThrowed = false;

    // Private fields
    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;
    private MeshCollider meshCollider;
    private Rigidbody rigidbody;

    // Unity methods
    private void Awake() {
        this.meshRenderer = GetComponent<MeshRenderer>();
        this.meshFilter = GetComponent<MeshFilter>();
        this.meshCollider = GetComponent<MeshCollider>();

        if(this.meshCollider != null) {
            this.meshCollider.convex = true;
        }

        this.rigidbody = GetComponent<Rigidbody>();

        transform.parent = GameObject.Find("_Dynamic").transform;
    }

    // Public methods
    public void SetItem(Item item, int amount = 0) {
        this.Data = item;
        this.Amount = amount;
        this.meshFilter.mesh = item.Model; 
        this.meshCollider.sharedMesh = item.Model;
        this.meshRenderer.sharedMaterials = item.Materials;
    }

    public Rigidbody GetRigidbody() { return this.rigidbody; }
    public MeshRenderer GetMeshRenderer() { return this.meshRenderer; }
    public MeshFilter GetMeshFilter() { return this.meshFilter; }
    public MeshCollider GetMeshCollider() { return this.meshCollider; }

    // Private methods
    #if UNITY_EDITOR
    private void OnValidate() {
        if(Data != null) {
            SetItem(Data);
        }
    }
    #endif
}