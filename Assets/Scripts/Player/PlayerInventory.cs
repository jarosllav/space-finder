using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory {
    // Events
    public static event Action<InventoryItem> onEquipItem;
    public static event Action<InventoryItem> onEquipQuickSlot; //TODO: not used
    public static event Action<InventoryItem> onTakeItem;
    public static event Action<InventoryItem> onDropItem;
    public static event Action<InventoryItem> onConsumeItem;
    public static event Action<InventoryItem, int> onAddItem;
    public static event Action<InventoryItem, int> onRemoveItem;

    // Public fields

    // Private fields
    private Quickbar quickbar;

    private float maxWeight;
    private float weight;
    private int maxSlots;
    private InventoryItem[] items;
    private InventoryItem equippedItem;

    public PlayerInventory(int maxSlots, int quickSlots, float maxWeight) {
        this.quickbar = new Quickbar(quickSlots);

        this.maxSlots = maxSlots;
        this.maxWeight = maxWeight;

        this.items = new InventoryItem[maxSlots];
        for(int i = 0; i < maxSlots; ++i) {
            this.items[i] = new InventoryItem() {
                Slot = i,
                Amount = 0,
                IsEmpty = true,
                Data = null
            };
        }

        // Walk around for saving
        GameObject.FindObjectOfType<InventoryGUI>().FillInventory(maxSlots);
    }

    // Public methods
    public void EquipItem(Item item) {
        InventoryItem invItem = this.getInventoryItem(item);
        
        //TODO: unequip?
        if(this.equippedItem != invItem) {}

        this.equippedItem = invItem;
        onEquipItem?.Invoke(invItem);
    }
    public void TakeItem(Item item, int amount) {
        InventoryItem invItem = null;
        int findItem = this.getInventoryItemSlot(item);

        if(findItem > -1 && this.items[findItem].Amount < this.items[findItem].Data.MaxStack) {
            invItem = this.items[findItem];

            if(invItem.Amount + amount > invItem.Data.MaxStack) {
                int delta = amount - (invItem.Data.MaxStack - invItem.Amount);
                invItem.Amount = invItem.Data.MaxStack;
                this.TakeItem(item, delta);
            }
            else {
                invItem.Amount += amount;
            }
        }
        else {
            int freeSlot = this.GetFirstFreeSlot();
            if(freeSlot == -1)
                return;

            invItem = this.items[freeSlot];
            invItem.Data = item;
            invItem.Amount = amount;
            invItem.IsEmpty = false;
        }

        weight += item.Weight * amount;
        onTakeItem?.Invoke(invItem);
    }
    public void DropItem(Item item, Vector3 position, Vector3 direction) {
        int invItemSlot = this.getInventoryItemSlot(item);
        if(invItemSlot > -1) {
            InventoryItem invItem = this.items[invItemSlot];

            int amount = invItem.Amount;
            weight -= item.Weight * amount;

            int quickSlot = this.quickbar.HasItemAssigned(item);
            if(quickSlot > -1) {
                this.quickbar.UnassignSlot(quickSlot);
            }
            
            this.items[invItemSlot].Data = null;
            this.items[invItemSlot].IsEmpty = true;

            GameObject objItem = new GameObject("WorldItem(" + item.Code + ")");
            objItem.transform.localScale *= 0.3f;
            objItem.transform.position = position;
            objItem.layer = LayerMask.NameToLayer("Ignore Player Collision");
            objItem.tag = "WorldItem";

            WorldItem worldItem = objItem.AddComponent<WorldItem>();
            worldItem.SetItem(item, amount);
            worldItem.GetRigidbody().AddForce(direction * 2f, ForceMode.Impulse);
            worldItem.IsThrowed = true;
            
            onDropItem?.Invoke(invItem);
        }
    }
    public void MoveItem(int slot, int newSlot) {
        InventoryItem invItem = this.items[slot];
        this.items[slot] = this.items[newSlot];
        this.items[newSlot] = invItem;
    }
    public void ConsumeItem(Item item) {
        int itemSlot = this.getInventoryItemSlot(item);
        if(itemSlot > -1) {
            InventoryItem invItem = this.items[itemSlot];
            if(invItem.Data.IsConsumable) {
                PlayerInventory.onConsumeItem?.Invoke(invItem);

                //TODO: Make remove method
                if(invItem.Data.DestoryAtConsume) {
                    invItem.Amount -= 1;
                    weight -= invItem.Data.Weight;

                    PlayerInventory.onRemoveItem?.Invoke(invItem, 1); //TODO: onRemoveItem, onAddItem

                    if(invItem.Amount <= 0) {
                        this.items[itemSlot].Data = null;
                        this.items[itemSlot].IsEmpty = true;
                    }                    
                }

            }
        }
    }

    public bool HasItem(string itemName) {
        foreach(InventoryItem invItem in items) {
            if(invItem.Data.Code == itemName || invItem.Data.Name == itemName) {
                return true;
            }
        }
        return false;
    }
    public bool HasItem(Item item) {
        foreach(InventoryItem invItem in items) {
            if(invItem.Data.Code == item.Code || invItem.Data.Name == item.Name) {
                return true;
            }
        }
        return false;
    }
    public Item GetItem(string itemName) {
        foreach(InventoryItem invItem in items) {
            if(invItem.Data.Code == itemName || invItem.Data.Name == itemName) {
                return invItem.Data;
            }
        }
        return null;
    }
    public InventoryItem GetItemBySlot(int slotId) {
        return this.items[slotId];
    }

    public Quickbar GetQuickbar() { return this.quickbar; }
    public int GetMaxSlots() { return this.maxSlots; }
    public float GetWeight() { return this.weight; }
    public float GetMaxWeight() { return this.maxWeight; }
    public InventoryItem[] GetItems() { return this.items; }
    public InventoryItem GetEquippedItem() { return this.equippedItem; }

    public int GetFirstFreeSlot() {
        for(int i = 0; i < this.maxSlots; ++i) {
            if(this.items[i].IsEmpty)
                return i;
        }

        return -1;
    }
    
    public void LoadFromSave(List<ItemSerializable> itemSerializables) {
        foreach(ItemSerializable itemSerializable in itemSerializables) {
            this.TakeItem(Database.GetItemByCode(itemSerializable.Code), itemSerializable.Amount);
        }
    }

    // Private methods
    private int getInventoryItemSlot(Item item) {
        for(int i = 0; i < this.maxSlots; ++i) {
            if(!this.items[i].IsEmpty && this.items[i].Data.Code == item.Code) {
                if(this.items[i].Amount >= this.items[i].Data.MaxStack)
                    continue;

                return i;
            }
        }
        return -1;
    }
    private InventoryItem getInventoryItem(Item item) {
        int slot = this.getInventoryItemSlot(item);
        return slot > -1 ? this.items[slot] : null;
    }
}