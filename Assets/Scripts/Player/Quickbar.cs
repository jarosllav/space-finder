using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quickbar {
    // Events
    public static event Action<int> onCreate;
    public static event Action<InventoryItem, int> onAssign;
    public static event Action<InventoryItem, int> onEquipSlot;
    public static event Action<int> onUnequipSlot;

    // Private fields
    private int maxSlots;
    private int equippedSlot;
    private InventoryItem[] slots;

    public Quickbar(int maxSlots) {
        this.equippedSlot = -1;
        this.maxSlots = maxSlots;
        this.slots = new InventoryItem[maxSlots];

        Quickbar.onCreate?.Invoke(maxSlots);
    }

    /// <summary>
    /// Assign item from inventory to given slot.
    /// </summary>
    /// <param name="item">Inventory item instance</param>
    /// <param name="slot">Slot index</param>
    public void AssignSlot(InventoryItem item, int slot) {
        if(slot < this.maxSlots && slot >= 0) {
            this.slots[slot] = item;
            Quickbar.onAssign?.Invoke(item, slot);
        }
    }

    /// <summary>
    /// Unassign item from slot.
    /// </summary>
    /// <param name="slot">Slot index</param>
    public void UnassignSlot(int slot) {
        if(slot < this.maxSlots && slot >= 0) {
            if(this.equippedSlot == slot) {
                this.Unequip();
            }

            this.slots[slot] = null;
            Quickbar.onAssign?.Invoke(null, slot);
        }
    }

    /// <summary>
    /// Checks if item is assigned to any slot.
    /// </summary>
    /// <param name="item">Item data instance</param>
    /// <returns>If item is assigned returns proper slot, otherwise returns -1.</returns>
    public int HasItemAssigned(Item item) {
        for(int i = 0; i < this.maxSlots; ++i) {
            if(this.slots[i] != null && this.slots[i].Data == item)
                return i;
        }
        return -1;
    }

    /// <summary>
    /// Equips given slot as active tool/item.
    /// </summary>
    /// <param name="slot">Slot index</param>
    /// <returns>Returns true if the item has beed equipped.</returns>
    public bool EquipSlot(int slot) {
        if(slot < 0 || slot > this.maxSlots)
            return false;
        if(this.slots[slot] == null)
            return false;

        if(this.equippedSlot == slot) {
            this.Unequip();
            return false;
        }

        this.equippedSlot = slot;
        Quickbar.onEquipSlot?.Invoke(this.slots[slot], slot);

        return true;
    }

    /// <summary>
    /// Unequips active quickbar slot.
    /// </summary>
    public void Unequip() {
        if(this.equippedSlot == -1)
            return;
        int slot = this.equippedSlot;
        this.equippedSlot = -1;
        Quickbar.onUnequipSlot?.Invoke(slot);
    }

    public InventoryItem GetEquippedSlot() {
        if(equippedSlot > -1)
            return this.slots[this.equippedSlot];
        return null;
    }

    //public void SetMaxSlots(int maxSlots) {}
    public int GetMaxSlots() { return this.maxSlots; }
}