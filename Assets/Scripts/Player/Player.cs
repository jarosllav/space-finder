using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEditor;
using System;
using System.Linq;

public enum DamageType {
    FALL,
    NONE
}

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
    // Events
    static public event Action<int, DamageType> onTakeDamage;
    static public event Action<int, int> onChangeHealth;
    static public event Action<int, int> onChangeMaxHealth;
    static public event Action<int, int> onChangeStamina;
    static public event Action onDead;

    // Private fields
    private CharacterController controller;
    private PlayerMovement movement;
    private PlayerInventory inventory;
    
    [SerializeField] private FollowCamera camera;

    [Header("Statistics")]
    [SerializeField] private int health = 100;
    [SerializeField] private int maxHealth = 100;
    [SerializeField] private int stamina = 100;
    [SerializeField] private int maxStamina = 100;

    [Header("Movement")]
    [SerializeField] private float speed;
    [SerializeField] private float sprintModifier;
    [SerializeField] private float jumpForce;
    [SerializeField] private float fallDistance;
    [SerializeField] private Transform headTransform;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask collisionMask;

    [Header("Inventory")]
    [SerializeField] private int maxSlots;
    [SerializeField] private int quickSlots;
    [SerializeField] private float maxWeight;

    [Header("Camera control")]
    [SerializeField] private float cameraSensivity = 0.25f;
    [SerializeField] private float cameraDistance = 3f;

    [Header("Gameplay")]
    [SerializeField] private bool autoCollecting = false;
    [SerializeField] private float autoCollectDistance = 3f;
	[SerializeField] private int shovelRadius = 0;

    // Unity methods
    private void Awake() {
        this.movement = new PlayerMovement(GetComponent<CharacterController>(), GetComponentInChildren<Animator>());
        this.movement.Speed = speed;
        this.movement.JumpForce = jumpForce;
        this.movement.SprintModifier = sprintModifier;
        this.movement.FallDistance = fallDistance;
        this.movement.SetGroundCheck(groundCheck);

        this.inventory = new PlayerInventory(maxSlots, quickSlots, maxWeight);
        PlayerInventory.onConsumeItem += this.handleConsumeItems;

        this.camera?.SetSensivity(cameraSensivity);
        this.camera?.SetDistance(cameraDistance);

        this.controller = GetComponent<CharacterController>();
    }

    private void Update() {
        this.handleInput();

        if(this.autoCollecting)
            this.processAutoCollecting();
    }

    private void FixedUpdate() {
        this.movement.Update();
    }

    // Public methods

    /// <summary>
    /// Deal damage to player (if players health is below zero invokes onDead event)
    /// </summary>
    /// <param name="damage">Amount of damage</param>
    /// <param name="damageType">Damage type</param>
    public void TakeDamage(int damage, DamageType damageType) {
        int hp = this.health - damage;

        if(hp <= 0) {
            hp = this.maxHealth;
            Player.onDead?.Invoke();
        }

        this.SetHealth(hp);
        Player.onTakeDamage?.Invoke(damage, damageType);
    }

    public void TakeItem(Item item, int amount) {
        inventory.TakeItem(item, amount);
    }

    public void DropItem(Item item) {
        inventory.DropItem(item, transform.position, transform.forward);
    }

    public void SetHealth(int health) {
        Player.onChangeHealth?.Invoke(health, this.health);
        this.health = health;
    }
    public void SetMaxHealth(int maxHealth) {
        Player.onChangeMaxHealth?.Invoke(maxHealth, this.maxHealth);
        this.maxHealth = maxHealth;
    }
    public void SetStamina(int stamina) { this.stamina = stamina; }
    public void SetMaxStamina(int maxStamina) { this.maxStamina = maxStamina; }
    public int GetHealth() { return this.health; }
    public int GetMaxHealth() { return this.maxHealth; }
    public int GetStamina() { return this.stamina; }
    public int GetMaxStamina() { return this.maxStamina; }

    public PlayerInventory GetPlayerInventory() { return this.inventory; }
    public PlayerMovement GetPlayerMovement() { return this.movement; }

    /// <summary>
    /// Loads player data from save
    /// </summary>
    /// <param name="playerSerializable"></param>
    public void LoadFromSave(PlayerSerializable playerSerializable) {
        if(this.controller != null)
            controller.enabled = false;

        this.transform.position = new Vector3(playerSerializable.X, playerSerializable.Y, playerSerializable.Z);
        this.transform.rotation = Quaternion.Euler(playerSerializable.RotX, playerSerializable.RotY, playerSerializable.RotZ);

        if(this.controller != null)
            controller.enabled = true;
    }

    // Private methods

    /// <summary>
    /// Event handler for consuming items by player.
    /// </summary>
    private void handleConsumeItems(InventoryItem invItem) {
        Item item = invItem.Data;
        switch(item.Code.ToLower()) {
            case "grass": 
                this.SetHealth(this.health + 2);
                break;
        }
    }

    /// <summary>
    /// Handles user input
    /// </summary>
    private void handleInput() {
        // Movement input
        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        
        if(this.movement.IsGrounded && Input.GetButtonDown("Jump")) 
            input.y = 1;

        this.movement.SetInput(input);

        // Sprinting
        if(Input.GetButton("Sprint")) {
            this.movement.IsSprinting = true;
        }

        // Camera-player distance manipulation
        if (Input.GetAxis("Mouse ScrollWheel") > 0f ) 
            this.camera.SetDistance(this.camera.GetDistance() - 0.15f);
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f ) 
            this.camera.SetDistance(this.camera.GetDistance() + 0.15f);

        // Opening inventory
        if(Input.GetButtonDown("Inventory")) {
            InventoryGUI invGUI = FindObjectOfType<InventoryGUI>();
            invGUI.Toggle();
        }

        // Escape menu
        if(Input.GetKeyDown(KeyCode.Escape)) {
            EscapeGUI escapeMenu = FindObjectOfType<EscapeGUI>();
            if(escapeMenu != null) {
                escapeMenu.Toggle();
            }
        }

        // Handle quickslots
        if(Input.GetButtonDown("Quickslot 1")) {
            this.inventory.GetQuickbar().EquipSlot(0);
        }
        else if(Input.GetButtonDown("Quickslot 2")) {
            this.inventory.GetQuickbar().EquipSlot(1);
        }
        else if(Input.GetButtonDown("Quickslot 3")) {
            this.inventory.GetQuickbar().EquipSlot(2);
        }

        // Picking world items
        if(Input.GetButtonDown("Pick")) {
            RaycastHit hit;
            int layerMask = ~(1 << LayerMask.NameToLayer("Player"));
            if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 7f, layerMask)) {
                if(hit.collider.tag == "WorldItem") {
                    WorldItem worldItem = hit.collider.gameObject.GetComponent<WorldItem>();
                    this.inventory.TakeItem(worldItem.Data, worldItem.Amount);
                    Destroy(worldItem.gameObject);
                }
            }
        }

        // Using tool from quickbar
        if(!EventSystem.current.IsPointerOverGameObject()) {
            if(Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) {
                RaycastHit hit;
                int layerMask = ~(1 << LayerMask.NameToLayer("Player"));

                if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 7f, layerMask)) {
                    Item equippedItem = this.inventory.GetQuickbar().GetEquippedSlot()?.Data;
                    
                    if(equippedItem) {
                        ItemType itemType = equippedItem.Type;

                        if(itemType == ItemType.SHOVEL) {
                            InfiniteWorld world = FindObjectOfType<InfiniteWorld>();
                            if(world) {
                                Vector3 point = hit.point;

                                if(Input.GetMouseButtonDown(0)) {
                                    world.LowerTerrain(point, shovelRadius);
                                }
                                else if(Input.GetMouseButtonDown(1)) {
                                    world.HigherTerrain(point, shovelRadius);

                                    Vector3 position = world.SnapToTerrain(this.transform.position);
                                    if(position != Vector3.zero) {
                                        this.controller.enabled = false;
                                        this.transform.position = position + new Vector3(0f, 1f, 0f);
                                        this.controller.enabled = true;
                                    }
                                }
                            }
                        }
                        else if(Input.GetMouseButtonDown(0) && itemType == ItemType.PICKAXE) {
                            if(hit.transform.tag == "WorldResource") {
                                WorldResource resource = hit.collider.gameObject.GetComponentInParent<WorldResource>();
                                if(resource == null)
                                    resource = hit.collider.gameObject.GetComponent<WorldResource>();

                                resource.Hit(1);

                                if(resource.IsDestroyed) {
                                    Item dropItem = resource.DropItem;
                                    this.inventory.TakeItem(dropItem, resource.DropAmount);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Looking for WorldItem's which player can auto-collect.
    /// </summary>
    private void processAutoCollecting() {
        WorldItem[] worldItems = GameObject.FindObjectsOfType<WorldItem>();
        for(int i = 0; i < worldItems.Length; ++i) {
            if(!worldItems[i].IsThrowed) {
                float dst = Vector2.Distance(worldItems[i].transform.position, transform.position);
                if(dst <= this.autoCollectDistance) {
                    WorldItem worldItem = worldItems[i];

                    this.inventory.TakeItem(worldItem.Data, worldItem.Amount);
                    GameObject.Destroy(worldItem.gameObject);
                }
            }
        }
    }

    #if UNITY_EDITOR
    private void OnValidate() {
        if(movement != null) {
            movement.JumpForce = jumpForce;
            movement.Speed = speed;
            movement.CollisionMask = collisionMask;
        }

        camera?.SetSensivity(cameraSensivity);
        camera?.SetDistance(cameraDistance);
    }

    private void OnDrawGizmos() {
        Gizmos.DrawSphere(groundCheck.position, 0.25f);
    }
    #endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(Player))]
public class PlayerEditor : Editor {
    private static bool showCheats = true;
    private static string giveCode = ""; 
    private static int giveAmount = 0; 

    private static string saveName = "test";
    private static string loadName = "test";

    public override void OnInspectorGUI() {
        Player player = (Player)target;
        DrawDefaultInspector();
#region cheats
        GUILayout.Space(10);
        GUILayout.Label("Cheats", EditorStyles.boldLabel);

        GUILayout.Label("Give item");
        
        EditorGUILayout.BeginHorizontal();

        giveCode = EditorGUILayout.TextField(giveCode);
        giveAmount = EditorGUILayout.IntField(giveAmount);

        EditorGUILayout.EndHorizontal();

        if(GUILayout.Button("Give")) {
            Item item = Database.GetItemByCode(giveCode);
            if(item != null) {
                player.TakeItem(item, giveAmount);
            }
        }
#endregion
#region saveSystem
        GUILayout.Space(10);
        GUILayout.Label("Save system", EditorStyles.boldLabel);

        EditorGUILayout.BeginHorizontal();
        
        saveName = EditorGUILayout.TextField(saveName);
        if(GUILayout.Button("Save")) {
            SaveSystem.Save(saveName, player, FindObjectOfType<InfiniteWorld>());
        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        
        loadName = EditorGUILayout.TextField(loadName);
        if(GUILayout.Button("Load")) {
            //var chunks = SaveSystem.Load(loadName, player, FindObjectOfType<InfiniteWorld>());
        }

        EditorGUILayout.EndHorizontal();
#endregion
    }
}
#endif