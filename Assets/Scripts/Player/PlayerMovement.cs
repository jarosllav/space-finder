using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement {
    // Public fields
    public float Speed;
    public float JumpForce;
    public float SprintModifier;
    public float FallDistance;

    public bool IsJumped = false;
    public bool IsGrounded = false;
    public bool IsSprinting = false;

    public LayerMask CollisionMask;

    // Private fields
    private CharacterController controller;
    private Animator animator;
    private Transform groundCheck;
    private Vector3 velocity = new Vector3(0, 0, 0);

    public PlayerMovement(CharacterController controller, Animator animator) {
        this.controller = controller;
        this.animator = animator;
    }

    // Public methods

    /// <summary>
    /// Updates player physics and move controller.
    /// </summary>
    public void Update() {
        Vector3 move = new Vector3(this.velocity.x, 0, this.velocity.z) * Speed;

        if(this.IsSprinting)
            move *= this.SprintModifier;
        
        if(this.velocity.x != 0f || this.velocity.z != 0f)
            this.controller.transform.rotation = Quaternion.LookRotation (new Vector3(this.velocity.x, 0, this.velocity.z));

        if(this.groundCheck)
            this.updateGravity();
        
        if(move.x != 0.0f || move.z != 0.0f)  {
            if(this.IsSprinting)
                this.animator?.SetFloat("Speed", 2);
            else
                this.animator?.SetFloat("Speed", 1);
        }
        else
            this.animator?.SetFloat("Speed", 0);


        move.y = this.velocity.y;
        this.controller.Move(move * Time.deltaTime);
    }

    /// <summary>
    /// Sets velocity based on user input
    /// </summary>
    /// <param name="input">User input</param>
    public void SetInput(Vector3 input) {
        Vector3 horizontal = Camera.main.transform.right * input.x;
        Vector3 vertical = Camera.main.transform.forward * input.z;

        velocity.x = horizontal.x + vertical.x;
        velocity.z = horizontal.z + vertical.z;

        if(input.y > 0) {
            animator?.SetTrigger("Jump");
            velocity.y = JumpForce;
        }
    }

    public void SetGroundCheck(Transform groundCheck) { this.groundCheck = groundCheck; }

    // Private methods

    /// <summary>
    /// Update velocity by gravity and deal fall damage to player.
    /// </summary>
    private void updateGravity() {
        this.velocity.y += Physics.gravity.y * Time.fixedDeltaTime;
        
        int layer = 1 << LayerMask.NameToLayer("Terrain") | 1 << LayerMask.NameToLayer("Resources");
        bool wasFalling = !this.IsGrounded;
        
        this.IsGrounded = Physics.CheckSphere(this.groundCheck.position, 0.25f, layer, QueryTriggerInteraction.Ignore);
        if(this.IsGrounded && velocity.y < 0) {
            float fallDistance = Vector3.Distance(this.velocity, this.groundCheck.localPosition);

            if(wasFalling) { 
                if(fallDistance > this.FallDistance) {
                    Player player = this.controller.GetComponent<Player>();
                    if(player) {
                        int dmg = Mathf.FloorToInt((fallDistance - this.FallDistance) / 2);
                        player.TakeDamage(dmg, DamageType.FALL);
                    }
                }

                this.animator?.SetTrigger("Fall");
            }

            this.velocity.y = 0;
        }
    }
} 