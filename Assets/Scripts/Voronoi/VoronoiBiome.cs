﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using System.Linq;

public class VoronoiBiome
{
    // Private fields
    private Vector2Int size;
    private Dictionary<Vector2Int, Biome[]> biomes;
    private int biomesCount = 0;

    public VoronoiBiome(Vector2Int size) {
        this.size = size;
        this.biomes = new Dictionary<Vector2Int, Biome[]>();
    }

    // Public methods
    public void Generate(Vector2Int coords, int regionsCount, BiomeSettings[] biomeTypes) {
        Vector3[] points = this.shedulePointJob(coords, regionsCount);

        Biome[] newBiomes = new Biome[points.Length];
        for(int i = 0; i < points.Length; ++i) {
            int biomeRand = UnityEngine.Random.Range(0, biomeTypes.Length);

            Biome biome = new Biome {
                Id = this.biomesCount,
                Position = points[i],
                BiomeSettings = biomeTypes[biomeRand],
                BiomeSettingsId = biomeRand
            };
            newBiomes[i] = biome;

            ++this.biomesCount;
        }

        this.biomes.Add(coords, newBiomes);
    }

    public List<Biome> GetNeighbourBiomes(Vector2Int coords) {
        float minDistance = float.MaxValue;
        Vector2Int minKey = Vector2Int.zero; 
        foreach(var list in this.biomes) {
            float distance = Vector2Int.Distance(coords, list.Key);
            if(distance <= minDistance) {
                minDistance = distance;
                minKey = list.Key;
            }
        }

        List<Biome> biomesList = new List<Biome>();

        if(minDistance != float.MaxValue) {
            for(int i = -1; i <= 1; ++i) {
                for(int j = -1; j <= 1; ++j) {
                    biomesList.AddRange(this.biomes[minKey + new Vector2Int(i, j)]);
                }
            }
        }

        return biomesList;
    }

    public List<Biome> GetNeighbourBiomes(Vector3 position) {
        Vector2Int coords = new Vector2Int((int)(position.x / this.size.x), (int)(position.z / this.size.y));
        return this.GetNeighbourBiomes(coords);
    }

    public Biome GetClosestBiome(Vector3 position) {
        //TODO: load neighbour biome only if chunk is in voronoi border
        Vector2Int coords = new Vector2Int((int)(position.x / this.size.x), (int)(position.z / this.size.y));
        List<Biome> biomesList = this.GetNeighbourBiomes(coords);

        float minDistance = float.MaxValue;
        int minId = 0;
        for(int i = 0; i < biomesList.Count; ++i) {
            if(Vector3.Distance(position, biomesList[i].Position) < minDistance) {
                minDistance = Vector3.Distance(position, biomesList[i].Position);
                minId = i;
            }
        }

        return biomesList[minId];
    }

    public bool FindChunk(Vector2Int coords) {
        if(this.biomes.ContainsKey(coords))
            return true;
        return false;
    }

    public void Clear() {
        biomes?.Clear();
    }

    public Vector2Int GetSize() { return this.size; }
    public Dictionary<Vector2Int, Biome[]> GetBiomes() { return this.biomes; }
    public int GetBiomesCount() { return this.biomesCount; }

    // Private methods
    private Vector3[] shedulePointJob(Vector2Int coords, int regionsCount) {
        // Allocate points array
        NativeArray<Vector3> points = new NativeArray<Vector3>(regionsCount, Allocator.TempJob);

        // Define job        
        VoronoiJob job = new VoronoiJob();
        job.RegionsCount = regionsCount;
        job.Size = new float2(size.x, size.y);
        job.Offset = new float2(size.x * coords.x, size.y * coords.y);
        job.Seed = (uint)(this.biomes.Count + 1);
        job.Points = points;

        // Shedule job
        JobHandle jobHandle = job.Schedule();
        jobHandle.Complete();

        Vector3[] result = points.ToArray();

        // Deallocate array
        points.Dispose();

        return result;
    }
}
