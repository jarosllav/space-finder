using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class VoronoiSphere
{
    // Private fields
    private List<Tuple<Vector3, int>> centroids;

    public VoronoiSphere() {
        this.centroids = new List<Tuple<Vector3, int>>();
    }

    // Public methods
    public void Generate(int regionsCount, int biomesLength, float radius = 1f) {
        for(int i = 0; i < regionsCount; ++i) {
            Vector3 point = UnityEngine.Random.onUnitSphere * radius;
            this.centroids.Add(new Tuple<Vector3, int>(point, UnityEngine.Random.Range(0, biomesLength)));
        }
    }

    public Tuple<Vector3, int> GetClosestCentroid(Vector3 point) {
        float dst = float.MaxValue;
        int id = 0;
        for(int i = 0; i < this.centroids.Count; ++i) {
            if(Vector3.Distance(point, this.centroids[i].Item1) < dst) {
                dst = Vector3.Distance(point, this.centroids[i].Item1);
                id = i;
            }
        }
        return this.centroids[id];
    }

    public void Clear() {
        if(centroids != null)
            centroids.Clear();
    }

    public List<Tuple<Vector3, int>> GetCentroids() {
        return centroids;
    }
}
