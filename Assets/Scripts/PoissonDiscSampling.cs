﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
public class PoissonDiscSampling
{
    // Public fields
    public float Radius;
    public int Limit;
    public Vector2 Size;

    // Private fields
    private float cellSize;
    private Vector2[,] grid;
    private Vector2Int gridSize;

    public PoissonDiscSampling(Vector2 size, float radius, int limit) {
        this.Radius = radius;
        this.Limit = limit;
        this.Size = size;

        this.cellSize = Mathf.Floor(radius / Mathf.Sqrt(2));
        this.gridSize = new Vector2Int(Mathf.CeilToInt(size.x / this.cellSize) + 1, Mathf.CeilToInt(size.y / this.cellSize) + 1);
    }

    // Public methods
    public Vector3[] Generate() {
        this.grid = new Vector2[this.gridSize.x, this.gridSize.y];

        List<Vector3> points = new List<Vector3>();
        List<Vector3> active = new List<Vector3>();

        Vector3 point0 = new Vector3(Random.Range(0, this.Size.x), 0, Random.Range(0, this.Size.y));
        this.insertPoint(point0);

        points.Add(point0);
        active.Add(point0);

        while(active.Count > 0) {
            int id = Random.Range(0, active.Count);
            Vector3 point = active[id];
            
            bool found = false;
            for(int i = 0; i < this.Limit; ++i) {
                float theta = Random.Range(0, 360) * Mathf.Deg2Rad;
                
                float px = point.x + Random.Range(this.Radius, 2 * this.Radius) * Mathf.Sin(theta); 
                float pz = point.z + Random.Range(this.Radius, 2 * this.Radius) * Mathf.Cos(theta);

                Vector3 newPoint = new Vector3(px, 0, pz);
                if(!this.isValidPoint(newPoint))
                    continue; 

                points.Add(newPoint);
                active.Add(newPoint);
                this.insertPoint(newPoint);
                found = true;
                break;
            }

            if(!found) 
                active.RemoveAt(id);
        }

        return points.ToArray();
    }

    // Private methods
    private void insertPoint(Vector3 point) {
        int i = Mathf.FloorToInt(point.x / this.cellSize);
        int j = Mathf.FloorToInt(point.z / this.cellSize);
        this.grid[i, j] = point;
    }

    private bool isValidPoint(Vector3 point) {
        if(point.x < 0 || point.x > this.Size.x || point.z < 0 || point.z > this.Size.y)
            return false;
        
        int x = Mathf.FloorToInt(point.x / this.cellSize);
        int y = Mathf.FloorToInt(point.z / this.cellSize);
        
        int i0 = (x - 1 > 0) ? x - 1 : 0;
        int i1 = (x + 1 > this.grid.GetLength(0) - 1) ? this.grid.GetLength(0) - 1 : x + 1;
        int j0 = (y - 1 > 0) ? y - 1 : 0;
        int j1 = (y + 1 > this.grid.GetLength(1) - 1) ? this.grid.GetLength(1) - 1 : y + 1;

        for(int i = i0; i <= i1; ++i) {
            for(int j = j0; j <= j1; ++j) {
                float dst = Mathf.Sqrt(Mathf.Pow(this.grid[i, j].x - point.x, 2) - Mathf.Pow(this.grid[i, j].y - point.z, 2));
                if(dst < this.Radius) {
                    return false;
                }
            }
        }

        return true;
    }
}
*/

/*public class PoissonDiscSampling {
    // Private fields
    private float cellSize;
    private float radius;

    // Public fields
    public Vector2 Offset;
    public Vector2 Size;

    public PoissonDiscSampling() {

    }

    public List<Vector2> Generate() {
        List<Vector2> points = new List<Vector2>();

        return points;
    }

    private bool isValidate(Vector2 point) {
        return false;
    }
}*/

public static class PoissonDiscSampling
{
    public static List<Vector2> Generate(float radius, Vector2 sampleRegionSize, int numSamplesBeforeRejection = 30) {
        float cellSize = radius / Mathf.Sqrt(2);
        int[,] grid = new int[Mathf.CeilToInt(sampleRegionSize.x / cellSize), Mathf.CeilToInt(sampleRegionSize.y / cellSize)];
        List<Vector2> points = new List<Vector2>();
        List<Vector2> spawnPoints = new List<Vector2>();

        spawnPoints.Add(new Vector2(sampleRegionSize.x / 2, sampleRegionSize.y / 2));
        while (spawnPoints.Count > 0) {
            int spawnIndex = Random.Range(0, spawnPoints.Count);
            Vector2 spawnCentre = spawnPoints[spawnIndex];
            bool candidateAccepted = false;

            for (int i = 0; i < numSamplesBeforeRejection; ++i) {
                float angle = Random.value * Mathf.PI * 2;
                Vector2 direction = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
                Vector2 candidate = spawnCentre + direction * Random.Range(radius, 2 * radius);

                if(IsValid(candidate, sampleRegionSize, cellSize, radius, points, grid)) {
                    points.Add(candidate);
                    spawnPoints.Add(candidate);
                    grid[(int)(candidate.x / cellSize), (int)(candidate.y / cellSize)] = points.Count;
                    candidateAccepted = true;
                    break;
                }
            }

            if (!candidateAccepted)
                spawnPoints.RemoveAt(spawnIndex);
        }

        return points;
    }

    public static bool IsValid(Vector2 candidate, Vector2 sampleRegionSize, float cellSize, float radius, List<Vector2> points, int[,] grid) {
        if (candidate.x >= 0 && candidate.x < sampleRegionSize.x && candidate.y >= 0 && candidate.y < sampleRegionSize.y) {
            int cellX = (int)(candidate.x / cellSize);
            int cellY = (int)(candidate.y / cellSize);
            for (int x = Mathf.Max(0, cellX - 2); x <= Mathf.Min(cellX + 2, grid.GetLength(0) - 1); ++x) {
                for (int y = Mathf.Max(0, cellY - 2); y <= Mathf.Min(cellY + 2, grid.GetLength(1) - 1); ++y) {
                    int pointIndex = grid[x, y] - 1;
                    if (pointIndex != -1) {
                        float distance = (candidate - points[pointIndex]).sqrMagnitude;
                        if (distance < radius * radius) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
}