[System.Serializable]
public class ItemSerializable {
    public int Slot;
    public int Amount;
    public string Code;
}