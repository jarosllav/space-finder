[System.Serializable]
public class PlayerSerializable {
    public float X;
    public float Y;
    public float Z;

    public float RotX; 
    public float RotY; 
    public float RotZ; 
}