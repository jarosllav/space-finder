[System.Serializable]
public class ChunkSerializable {
    public int X;
    public int Y;
    public float[] Noise;
}