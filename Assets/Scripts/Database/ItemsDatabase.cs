﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Database/Items")]
public class ItemsDatabase : ScriptableObject
{
    public List<Item> Items;
}
