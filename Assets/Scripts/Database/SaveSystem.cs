﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveData {
    public PlayerSerializable Player;
    public WorldSerializable World;
    public List<ItemSerializable> Items;
    public List<ChunkSerializable> Chunks;
}

public class SaveSystem
{
    /*
        Events
    */
    public static event Action onSave;
    public static event Action onLoad;

    /*

        Saving methods

    */

    public static bool Save(string name, Player player, InfiniteWorld world) {
        string path = Application.persistentDataPath + "/saves/" + name;

        if(!Directory.Exists(path))
            Directory.CreateDirectory(path);

        SaveWorld(path, world);
        SavePlayer(path, player);
        SaveInventory(path, player.GetPlayerInventory());
        SaveChunks(path, world);

        SaveSystem.onSave?.Invoke();

        Debug.Log("Saving done.");

        return true;
    }

    public static void SavePlayer(string path, Player player) {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream file = new FileStream(path + "/player.dat", FileMode.Create, FileAccess.Write, FileShare.None);

        Vector3 position = player.transform.position;
        Vector3 rotation = player.transform.rotation.eulerAngles;

        PlayerSerializable playerSerializable = new PlayerSerializable() {
            X = position.x, Y = position.y, Z = position.z,
            RotX = rotation.x, RotY = rotation.y, RotZ = rotation.z
        };

        formatter.Serialize(file, playerSerializable);
        file.Close();
    }

    public static void SaveInventory(string path, PlayerInventory inventory) {
        BinaryFormatter formatter = new BinaryFormatter();

        InventoryItem[] items = inventory.GetItems();
        if(items.Length == 0)
            return;

        FileStream file = new FileStream(path + "/player_inventory.dat", FileMode.Create, FileAccess.Write, FileShare.None);
        
        List<ItemSerializable> itemsSerializable = new List<ItemSerializable>();

        foreach(InventoryItem inventoryItem in items) {
            ItemSerializable itemSerializable = new ItemSerializable();

            if(inventoryItem == null || inventoryItem.IsEmpty)
                continue;

            itemSerializable.Code = inventoryItem.Data.Code;
            itemSerializable.Amount = inventoryItem.Amount;
            itemSerializable.Slot = inventoryItem.Slot;

            itemsSerializable.Add(itemSerializable);
        }

        formatter.Serialize(file, itemsSerializable);
        file.Close();
    }

    public static void SaveWorld(string path, InfiniteWorld world) {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream file = new FileStream(path + "/world.dat", FileMode.Create, FileAccess.Write, FileShare.None);

        WorldSerializable worldSerializable = new WorldSerializable() {
            Seed = world.GetSeed()
        };

        formatter.Serialize(file, worldSerializable);
        file.Close();
    }

    public static void SaveChunks(string path, InfiniteWorld world) {
        if(!Directory.Exists(path + "/chunks"))
            Directory.CreateDirectory(path + "/chunks");

        BinaryFormatter formatter = new BinaryFormatter();

        HashSet<Chunk> modifiedChunks = world.GetModifiedChunks();

        foreach(Chunk chunk in modifiedChunks) {
            Vector2Int coords = chunk.GetCoords();

            string chunkPath = path + "/chunks/c-" + coords.x + "-" + coords.y + ".dat";
            FileStream file = new FileStream(chunkPath, FileMode.Create, FileAccess.Write, FileShare.None);
            
            ChunkSerializable chunkData = new ChunkSerializable() {
                X = coords.x,
                Y = coords.y,
                Noise = chunk.Data.Noise
            };

            formatter.Serialize(file, chunkData);
            file.Close();
        }

        world.ClearModifiedChunks();
    }

    /*

        Loading methods

    */

    public static SaveData Load(string name) {
        string path = Application.persistentDataPath + "/saves/" + name;

        if(!Directory.Exists(path)) {
            Directory.CreateDirectory(path);
            return null;
        }

        BinaryFormatter formatter = new BinaryFormatter();

        SaveData saveData = new SaveData();

        saveData.Player = LoadPlayer(path);
        saveData.World = LoadWorld(path);
        saveData.Items = LoadItems(path);
        saveData.Chunks = LoadChunks(path);

        SaveSystem.onLoad?.Invoke();

        return saveData;
    }

    public static PlayerSerializable LoadPlayer(string path) {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream file = new FileStream(path + "/player.dat", FileMode.Open, FileAccess.Read, FileShare.None);

        PlayerSerializable playerSerializable = (PlayerSerializable)formatter.Deserialize(file);
        file.Close();

        return playerSerializable;
    }

    public static WorldSerializable LoadWorld(string path) {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream file = new FileStream(path + "/world.dat", FileMode.Open, FileAccess.Read, FileShare.None);

        WorldSerializable worldSerializable = (WorldSerializable)formatter.Deserialize(file);
        file.Close();

        return worldSerializable;
    }

    public static List<ItemSerializable> LoadItems(string path) {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream file = new FileStream(path + "/player_inventory.dat", FileMode.Open, FileAccess.Read, FileShare.None);

        List<ItemSerializable> itemsSerializable = (List<ItemSerializable>)formatter.Deserialize(file);
        
        file.Close();

        return itemsSerializable;
    }

    public static List<ChunkSerializable> LoadChunks(string path) {
        if(!Directory.Exists(path + "/chunks"))
            return null;

        BinaryFormatter formatter = new BinaryFormatter();

        string[] chunksFiles = Directory.GetFiles(path + "/chunks");

        if(chunksFiles.Length == 0) 
            return null;

        List<ChunkSerializable> chunks = new List<ChunkSerializable>();

        foreach(string chunkPath in chunksFiles) {
            FileStream file = new FileStream(chunkPath, FileMode.Open, FileAccess.Read, FileShare.None);

            ChunkSerializable chunkSerializable = (ChunkSerializable)formatter.Deserialize(file);
            chunks.Add(chunkSerializable);

            file.Close();
        }

        return chunks;
    }

    /*
    
        Utilities methods
    
    */

    public static bool RemoveSave(string saveName) {
        string path = Application.persistentDataPath + "/saves/" + saveName;

        if(!Directory.Exists(path))
            return false;

        Directory.Delete(path, true);
        return true;
    }
    public static void RemoveAllSaves() {}

    public static string[] GetAllSavesFiles() {
        string[] savesDirectories = Directory.GetDirectories(Application.persistentDataPath + "/saves");
        string[] saves = new string[savesDirectories.Length];
        
        for(int i = 0; i < saves.Length; ++i) {
            DirectoryInfo dir = new DirectoryInfo(savesDirectories[i]);
            saves[i] = dir.Name;
        }

        return saves;
    }
}



/*

public static void SaveRegion(string path, InfiniteWorld world) {
        if(!Directory.Exists(path + "/chunks"))
            Directory.CreateDirectory(path + "/chunks");

        BinaryFormatter formatter = new BinaryFormatter();

        HashSet<Chunk> modifiedChunks = world.GetModifiedChunks();
        Dictionary<string, Queue<Chunk>> chunks = new Dictionary<string, Queue<Chunk>>();

        Vector2 regionSize = world.GetRegionSize();

        foreach(Chunk chunk in modifiedChunks) {
            Vector2Int coords = chunk.GetCoords();
            string regionName = "r" + (int)(coords.x / regionSize.x) + "-" + (int)(coords.y / regionSize.y);

            if(!chunks.ContainsKey(regionName)) 
                chunks.Add(regionName, new Queue<Chunk>());

            chunks[regionName].Enqueue(chunk);
        }

        world.ClearModifiedChunks();

        for(int i = 0 ; i < chunks.Count; ++i) {
            string regionPath = path + "/regions/" + chunks.ElementAt(i).Key + ".region";
            FileStream file = new FileStream(regionPath, FileMode.Create, FileAccess.Write, FileShare.None);

            RegionSerializable regionData = new RegionSerializable();

            foreach(Chunk chunk in chunks.ElementAt(i).Value) {
                Vector2Int coords = chunk.GetCoords();

                ChunkSerializable chunkData = new ChunkSerializable() {
                    X = coords.x,
                    Y = coords.y,
                    Noise = chunk.Data.Noise
                };

                regionData.Chunks.Add(chunkData);
            }

            formatter.Serialize(file, regionData);
            file.Close();
        }
    }

    public static List<ChunkSerializable> LoadRegion(string path) {
        if(!Directory.Exists(path + "/regions"))
            return null;

        BinaryFormatter formatter = new BinaryFormatter();

        string[] regions = Directory.GetFiles(path + "/regions");

        if(regions.Length == 0) 
            return null;

        List<ChunkSerializable> chunks = new List<ChunkSerializable>();

        foreach(string regionPath in regions) {
            FileStream file = new FileStream(regionPath, FileMode.Open, FileAccess.Read, FileShare.None);

            RegionSerializable regionSerializable = (RegionSerializable)formatter.Deserialize(file);
            chunks.AddRange(regionSerializable.Chunks);

            file.Close();
        }

        return chunks;
    }

    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public interface ISaveable {
    object GetProfile();
    void SetProfile(object profile);
}

[System.Serializable]
public class RegionSerializable { 

}

public class SaveSystem
{
    public int SectorSize = 4096;

    public static event Action onSave;
    public static event Action onLoad;

    /// <summary>
    /// Save entire game data (player, chunks)
    /// </summary>
    public static bool Save(string name, Player player, InfiniteWorld world) {
        string path = Application.persistentDataPath + "/saves/" + name;
        //Debug.Log("Saving data to " + path);

        if(!Directory.Exists(path))
            Directory.CreateDirectory(path);

        BinaryFormatter formatter = new BinaryFormatter();

        SaveChunks(path, world);

        return true;
    }

    public static void SaveChunks(string path, InfiniteWorld world) {
        // Save region

        /*

            [ Lookup table   ] [ Chunk coords, noise ]
            [ 0 - 4 * chunks ] [ x, y,       float[] ] [..] [..]

        *

        if(!Directory.Exists(path + "/regions"))
            Directory.CreateDirectory(path + "/regions");

        BinaryFormatter formatter = new BinaryFormatter();

        List<Chunk> modifiedChunks = world.GetModifiedChunks();
        Dictionary<string, Queue<Chunk>> chunks = new Dictionary<string, Queue<Chunk>>();

        Vector2 regionSize = world.GetRegionSize();

        for(int i = 0; i < modifiedChunks.Count; ++i) {
            Chunk chunk = modifiedChunks[i];
            Vector2Int coords = chunk.GetCoords();
            
            string regionName = "r" + (int)(coords.x / regionSize.x) + "-" + (int)(coords.y / regionSize.y);

            if(!chunks.ContainsKey(regionName)) 
                chunks.Add(regionName, new Queue<Chunk>());

            chunks[regionName].Enqueue(chunk);
        }

        world.ClearModifiedChunks();

        Vector3 chunkSize = world.GetChunkSize();

        int totalChunks = (int)(regionSize.x * regionSize.y);
        int halfX = (int)(chunkSize.x / 2f), halfZ = (int)(chunkSize.z / 2f);

        for(int i = 0 ; i < chunks.Count; ++i) {
            string regionPath = path + "/regions/" + chunks.ElementAt(i).Key + ".region";

            if(File.Exists(regionPath)) {
                // ...
                //return;
            }

            //FileStream file = new FileStream(regionPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            FileStream file = new FileStream(regionPath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);

            // Lookup table
            file.Write(new byte[4 * totalChunks], 0, 4 * totalChunks);

            foreach(Chunk chunk in chunks.ElementAt(i).Value) {
                Vector2Int coords = chunk.GetCoords();

                long position = file.Position;

                int offset = 4 * (int)( (coords.x % halfX) + (coords.y % halfZ * halfX) );
                file.Position = offset;

                byte[] section = BitConverter.GetBytes((int)file.Position);
                file.Write(section, 0, 4);

                file.Position = position;

                // Write chunk coords
                file.Write(BitConverter.GetBytes(coords.x), 0, 4);
                file.Write(BitConverter.GetBytes(coords.y), 0, 4);

                // Write noise array
                byte[] noise = new byte[4 * chunk.Data.Noise.Length];
                Buffer.BlockCopy(chunk.Data.Noise, 0, noise, 0, noise.Length);
                file.Write(noise, 0, noise.Length);*

                formatter.Serialize(file, coords.x);
                formatter.Serialize(file, coords.y);
                formatter.Serialize(file, chunk.Data.Noise);
            }

            file.Close();
        }
    }

    public static object Load(string name, Player player, InfiniteWorld world) {
        string path = Application.persistentDataPath + "/saves/" + name;
        //Debug.Log("Saving data to " + path);

        if(!Directory.Exists(path))
            Directory.CreateDirectory(path);

        BinaryFormatter formatter = new BinaryFormatter();

        var chunks = LoadChunks(path, world);

        return true;
    }

    public static Dictionary<Vector2Int, float[]> LoadChunks(string path, InfiniteWorld world) {
        if(!Directory.Exists(path + "/regions"))
            return null;

        BinaryFormatter formatter = new BinaryFormatter();

        Dictionary<Vector2Int, float[]> chunks = new Dictionary<Vector2Int, float[]>();

        Vector2 regionSize = world.GetRegionSize();
        Vector3 chunkSize = world.GetChunkSize();

        int totalChunks = (int)(regionSize.x * regionSize.y);
        int halfX = (int)(chunkSize.x / 2f), halfZ = (int)(chunkSize.z / 2f);

        string[] regions = Directory.GetFiles(path + "/regions");

        if(regions.Length == 0) 
            return null;

        foreach(string regionPath in regions) {
            FileStream file = File.OpenRead(regionPath);

            //byte[] lookup = new byte[4 * totalChunks];
            //file.Read(lookup, 0, 4 * totalChunks);
            int lookupSize = 4 * totalChunks;

            for(int i = 0; i < lookupSize; i += 4) {
                byte[] section = new byte[4];
                file.Read(section, 0, 4);

                bool empty = section.All(single => single == 0);

                if(!empty) {
                    long position = file.Position;
                    int sectorPosition = BitConverter.ToInt32(section, 0);

                    file.Position = sectorPosition;

                    Debug.Log(file.Position);

                    //int x = (int)formatter.Deserialize(file);
                    //int y = (int)formatter.Deserialize(file);
                    float[] noise = (float[])formatter.Deserialize(file);

                    //Debug.Log(x + " " + y + " chunk loading");

                }
            }   

            file.Close();
        }

        return chunks;
    }
}
*/