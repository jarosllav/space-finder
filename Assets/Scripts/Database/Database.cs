﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Database : MonoBehaviour
{
    static private Database instance;

    // Private fields
    [SerializeField] private ItemsDatabase itemsDb;

    // Unity methods
    private void Awake() {
        if(instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }

    // Public methods
    public static List<Item> GetAllItems() { return instance.itemsDb.Items; }

    public static Item GetItemByCode(string code) {
        foreach(Item item in instance.itemsDb.Items) {
            if(item.Code.ToLower() == code.ToLower()) {
                return item;
            }
        }
        return null;
    }
}
