﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseGenerator
{   
    // Private fields
    private Noise noise = new Noise();

    // Public fields
    public float Scale = 8f;
    public int Octaves = 4;
    public float Lacunarity = 2f;
    public float Persistence = 0.2f;

    // Conctructor
    public NoiseGenerator() {
        // ...
    }

    // Public methods

    /// <summary>
    /// Evaulates noise value at given position
    /// </summary>
    /// <param name="vertex">Point position</param>
    /// <returns>Noise value between 0 and 1</returns>
    public float Evaluate(Vector3 vertex) {
        float noiseValue = 0f;
        float frequency = 1f;
        float amplitude = 1f;
        for(int octave = 0; octave < this.Octaves; ++octave) {
            float value = this.noise.Evaluate((vertex / this.Scale) * frequency);
            noiseValue += value * amplitude;
            amplitude *= this.Persistence;
            frequency *= this.Lacunarity;
        }
        return noiseValue;
    }

    /// <summary>
    /// Changes noise seed to random number
    /// </summary>
    public void Randomize() {
        this.noise.Randomize(Random.Range(0, 2137));
    }

    // Private methods
    // ...
}
