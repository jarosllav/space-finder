﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Planet : MonoBehaviour
{
    // Private fields
    private SphereWorld world;
    Vector3 pivot;

    // Public fields
    public Vector3 RotateSpeed;
    public float CameraSpeed = 1f;
    public Vector3 CameraPosition;
    public TMP_InputField WorldName;

    // Unity methods
    void Awake() {
        world = GetComponent<SphereWorld>();

        Camera camera = Camera.main;
        camera.transform.position = Vector3.zero;

        if(world)
            pivot = Vector3.one * world.Radius;
    }

    void Update() {
        Vector3 rotation = transform.localRotation.eulerAngles;
        transform.RotateAround(pivot, RotateSpeed, Time.deltaTime);

        if(Camera.main.transform.position != CameraPosition) {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, CameraPosition, CameraSpeed);
        }
    }

    // Public methods
    public void Explore() {
        string worldName = WorldName.text.ToString();
        GameManager.Instance.CreateNewWorld(worldName);
    }

    public void Randomize() {
        world.Generate();
    }

    public void BackToLobby() {
        GameManager.Instance.BackToMain();
    }
}
