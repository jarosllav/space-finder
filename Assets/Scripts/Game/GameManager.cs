﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum ScenesID {
    GAME = 0,
    MAINMENU = 1,
    PLANET = 2,
    WORLD = 3
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField] private GameObject loadingScreen;

    private List<AsyncOperation> scenesLoading = new List<AsyncOperation>();
    private Player player;
    private InfiniteWorld world;
    private SaveData saveData;

    private string worldName;
    private int activeScene;

    // Unity methods
    private void Awake() {
        if(Instance == null) 
            Instance = this;
        else 
            Destroy(gameObject);

        this.loadingScreen?.SetActive(false);
        SceneManager.LoadScene((int)ScenesID.MAINMENU, LoadSceneMode.Additive);
    }

    // Public methods
    public void StartNewGame() {
        this.loadScenes((int)ScenesID.PLANET, (int)ScenesID.MAINMENU);
    }

    public void CreateNewWorld(string worldName) {
        this.worldName = worldName;
        AsyncOperation createOperation = this.loadScenes((int)ScenesID.WORLD, (int)ScenesID.PLANET);
        createOperation.completed += this.onWorldCreated;
    }

    public void BackToMain() {
        this.loadScenes((int)ScenesID.MAINMENU, this.activeScene);
    }

    public void LoadGame(string saveName) {
        AsyncOperation loadOperation = this.loadScenes((int)ScenesID.WORLD, (int)ScenesID.MAINMENU);
        loadOperation.completed += this.onWorldLoaded;

        this.saveData = SaveSystem.Load(saveName);
        this.worldName = saveName;
    }

    public string GetWorldName() { return this.worldName; }

    // Private methods
    private void onWorldLoaded(AsyncOperation asyncOperation) {
        player = FindObjectOfType<Player>();
        world = FindObjectOfType<InfiniteWorld>();

        world.Init(saveData.World.Seed);

        List<ChunkSerializable> chunkSerializables = saveData.Chunks;
        if(chunkSerializables != null)
            world.LoadFromSave(chunkSerializables);

        PlayerSerializable playerSerializable = saveData.Player;
        player.LoadFromSave(playerSerializable);

        PlayerInventory inventory = this.player.GetPlayerInventory();
        inventory.LoadFromSave(saveData.Items);

        world.Generate();
    }

    private void onWorldCreated(AsyncOperation asyncOperation) {
        world = FindObjectOfType<InfiniteWorld>();
        player = FindObjectOfType<Player>();

        int seed = UnityEngine.Random.Range(0, 2137);

        world.Init(seed);
        world.Generate();
    }

    private AsyncOperation loadScenes(int loadId, int unloadId) {
        this.loadingScreen?.SetActive(true);

        this.scenesLoading.Add(SceneManager.UnloadSceneAsync(unloadId));

        AsyncOperation loadOperation = SceneManager.LoadSceneAsync(loadId, LoadSceneMode.Additive);
        this.scenesLoading.Add(loadOperation);

        StartCoroutine(getSceneLoadProgess());

        this.activeScene = loadId;

        return loadOperation;
    }

    private IEnumerator getSceneLoadProgess() {
        for(int i = 0; i < scenesLoading.Count; ++i) {
            while(!scenesLoading[i].isDone) {
                yield return null;
            }
        }

        this.scenesLoading.Clear();
        this.loadingScreen?.SetActive(false);
    }
}
