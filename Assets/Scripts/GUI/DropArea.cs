using System;
using System.Collections.Generic;
using UnityEngine;

public class DropArea : MonoBehaviour
{
	public event Action<Draggable> OnDropHandler;

	public void Drop(Draggable draggable) {
		OnDropHandler?.Invoke(draggable);
	}
}