﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMenuGUI : MonoBehaviour
{
    public Transform SavesContainer;
    public GameObject SavePrefab;

    private string[] saves;
    private GameObject[] savesGameObjects;

    private void OnEnable() {
        this.saves = SaveSystem.GetAllSavesFiles();

        foreach(string saveName in this.saves) {
            GameObject save = Instantiate(SavePrefab, Vector3.zero, Quaternion.identity);
            save.transform.parent = SavesContainer;
            save.transform.localScale = Vector3.one;
            save.GetComponent<SaveItemGUI>().SetName(saveName);
            save.GetComponent<SaveItemGUI>().PlayButton.onClick.AddListener(delegate { this.ChooseSave(saveName); });
            save.GetComponent<SaveItemGUI>().RemoveButton.onClick.AddListener(delegate { this.RemoveSave(saveName, save); });
        }
    }

    public void ChooseSave(string saveName) {
        GameManager.Instance.LoadGame(saveName);
    }

    public void RemoveSave(string saveName, GameObject saveObject) {
        SaveSystem.RemoveSave(saveName);
        Destroy(saveObject);
    }

}
