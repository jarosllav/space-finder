﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeGUI : MonoBehaviour
{
    private CursorLockMode lockMode;

    private void Awake() {
        this.Close();
    }

    public void Open() {
        this.lockMode = Cursor.lockState;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void Close() {
        Cursor.lockState = this.lockMode;
        Time.timeScale = 1f;
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void Toggle() {
        if(transform.GetChild(0).gameObject.activeSelf)
            Close();
        else
            Open();
    }

    public void OnResumeButton() {
        this.Close();
    }

    public void OnSaveButton() {
        SaveSystem.Save(GameManager.Instance.GetWorldName(), FindObjectOfType<Player>(), FindObjectOfType<InfiniteWorld>());
    }

    public void OnExitButton() {
        Application.Quit();
    }
}
