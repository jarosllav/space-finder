﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuGUI : MonoBehaviour
{
    public Button ContinueButton;

    public GameObject MainMenu;
    public GameObject LoadMenu;
    public GameObject SettingsMenu;

    private GameObject activeMenu;

    private void Awake() {
        this.openMenu(this.MainMenu);
    }

    private void OnEnable() {
        if(SaveSystem.GetAllSavesFiles().Length > 0)
            this.ContinueButton.interactable = true;
    }

    // Public methods
    public void BackToMain() {
        this.activeMenu.SetActive(false);
        this.openMenu(this.MainMenu);
    }

    public void OnPlayButton() {
        GameManager.Instance.StartNewGame();
    }

    public void OnContinueButton() {
        this.closeActiveMenu();
        this.openMenu(this.LoadMenu);
    }

    public void OnSettingsButton() {
        
    }

    public void OnExitButton() {
        Application.Quit();
    }

    private void closeActiveMenu() {
        if(activeMenu != null) {
            activeMenu.SetActive(false);
        }
    }

    private void openMenu(GameObject menu) {
        menu.SetActive(true);
        this.activeMenu = menu;
    }
}
