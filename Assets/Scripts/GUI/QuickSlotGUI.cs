﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class QuickSlotGUI : MonoBehaviour
{
    // Public fields
    public int Slot;
    public DropArea Area;
    public Image Background;
    public Image Texture;

    public Item Item;

    // Public methods
    public void SetItem(Item item) {
        this.Item = item;

        this.Texture.sprite = item.Image;
        this.Texture.gameObject.SetActive(true);
    }

    public void Clear() {
        this.Item = null;
        this.Texture.gameObject.SetActive(false);
    }
}
