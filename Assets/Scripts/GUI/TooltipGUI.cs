﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class TooltipGUI : MonoBehaviour
{
    public TextMeshProUGUI Header;
    public TextMeshProUGUI Description;
    public TextMeshProUGUI Type;
    public TextMeshProUGUI Consumable;

    public Vector3 Offset;

    private PointerEventData pointerEventData;
    private bool isOpen = false;

    private void Awake() {
        this.pointerEventData = new PointerEventData(EventSystem.current) {
            pointerId = -1
        };

        this.Close();
    }

    private void Update() {
        transform.position = Input.mousePosition + Offset;
        
        pointerEventData.position = Input.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(this.pointerEventData, results);

        foreach(RaycastResult result in results) {
            InventoryItemGUI itemGUI = result.gameObject.GetComponent<InventoryItemGUI>();            
            if(itemGUI != null && itemGUI.Item != null && !itemGUI.Item.IsEmpty) {
                if(!isOpen)        
                    this.Open();

                Item data = itemGUI.Item.Data;

                Header?.SetText(data.Name);
                Description?.SetText(data.Description);
                Type?.SetText("Type: " + data.Type.ToString());
                Consumable?.SetText("Is consumable: " + (data.IsConsumable ? "Yes" : "No"));

                return;
            }
        }

        if(isOpen)        
            this.Close();
    }

    public void Open() {
        isOpen = true;
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void Close() {
        isOpen = false;
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
