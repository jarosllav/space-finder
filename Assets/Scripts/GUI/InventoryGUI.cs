using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventoryGUI : MonoBehaviour {
    // Public fields
    public Canvas Canvas;
    public TextMeshProUGUI HeaderText;
    public TextMeshProUGUI WeightText;
    public Transform ItemsContainer;
    public GameObject ItemPrefab;
    public DropArea DropItemArea;
    
    // Private fields
    private InventoryItemGUI[] slots;
    private bool isOpen = false;

    private PlayerInventory playerInventory;
    
    // Unity methods
    private void Awake() {
        this.Close();
        DropItemArea.OnDropHandler += onDropOutside;

        PlayerInventory.onTakeItem += this.takeItem;
        PlayerInventory.onDropItem += this.dropItem;
        PlayerInventory.onRemoveItem += this.removeItem;
    }

    // Public methods
    public void FillInventory(int maxSlots) {
        this.slots = new InventoryItemGUI[maxSlots];
        
        for(int i = 0; i < maxSlots; ++i) {
            GameObject slot = Instantiate(ItemPrefab, Vector3.zero, Quaternion.identity, ItemsContainer);
            slot.transform.localScale = new Vector3(1, 1, 1);

            InventoryItemGUI invItem = slot.gameObject.GetComponent<InventoryItemGUI>();
            invItem.Hide();

            this.slots[i] = invItem;
        }
    }

    public void Choose() {
    }

    public void Open() {
        Cursor.lockState = CursorLockMode.None;

        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(true);
        }

        Time.timeScale = 0f;
        this.isOpen = true;
    }

    public void Close() {
        Cursor.lockState = CursorLockMode.Locked;
        
        for(int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        Time.timeScale = 1f;
        this.isOpen = false;
    }

    public void Toggle() {
        if(this.isOpen)
            Close();
        else
            Open();
    }

    // Private methods
    private void onDropOutside(Draggable draggable) {
        Player player = FindObjectOfType<Player>();
        if(player != null) {
            Item item = draggable.transform.GetComponentInParent<InventoryItemGUI>().Item.Data;
            player.DropItem(item);
        }
    }

    private void takeItem(InventoryItem item) {
        Debug.Log(item + " " + item.IsEmpty);
        InventoryItemGUI invItem = this.slots[item.Slot];
        
        invItem.Image.sprite = item.Data.Image;
        invItem.Count.SetText("x" + item.Amount);
        invItem.Item = item;
        invItem.Show();

        this.updateWeight();
    }

    private void dropItem(InventoryItem item) {
        InventoryItemGUI invItem = this.slots[item.Slot];
        invItem.Clear();
        this.updateWeight();
    }
    
    private void removeItem(InventoryItem item, int amount) {
        if(item.Amount <= 0) {
            dropItem(item);
            return;
        }

        InventoryItemGUI invItem = this.slots[item.Slot];
        invItem.Count.SetText("x" + item.Amount);

        this.updateWeight();
    }

    private void updateWeight() {
        PlayerInventory inventory = FindObjectOfType<Player>().GetPlayerInventory();
        float weight = (float)Mathf.Round(inventory.GetWeight() * 100f) / 100f;
        WeightText.SetText("Weight: " + weight + "/" + inventory.GetMaxWeight());
    }
}