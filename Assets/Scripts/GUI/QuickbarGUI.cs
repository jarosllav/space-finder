﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickbarGUI : MonoBehaviour
{
    // Public fields
    public GameObject QuickSlotPrefab;
    public Transform QuickSlotsContainer;
    
    // Private fields
    private int equippedId;
    private QuickSlotGUI[] quickSlots;

    // Unity methods
    private void Awake() {
        Quickbar.onCreate += onCreate;
        Quickbar.onAssign += onAssign;
        Quickbar.onEquipSlot += onEquip;
        Quickbar.onUnequipSlot += onUnequip;
    }

    // Public methods
    public void SetQuickSlot(int quickId, Item itemData) {
        
    }

    // Private methods
    private void onDrop(Draggable draggable) {
        InventoryItemGUI inventoryItem = draggable.transform.GetComponentInParent<InventoryItemGUI>();
        if(inventoryItem != null) {
            PlayerInventory inventory = FindObjectOfType<Player>().GetPlayerInventory();

            QuickSlotGUI quickslot = draggable.Area.GetComponent<QuickSlotGUI>();
            if(quickslot != null) {
                inventory.GetQuickbar().AssignSlot(inventoryItem.Item, quickslot.Slot);
            }
        }
    }

    private void onCreate(int maxSlots) {
        this.quickSlots = new QuickSlotGUI[maxSlots];

        if(QuickSlotPrefab == null)
            return;
        for(int i = 0; i < maxSlots; ++i) {
            GameObject quickslot = Instantiate(QuickSlotPrefab, Vector3.zero, Quaternion.identity);
            quickslot.transform.parent = QuickSlotsContainer;

            this.quickSlots[i] = quickslot.GetComponent<QuickSlotGUI>();
            this.quickSlots[i].Slot = i;
            this.quickSlots[i].Area.OnDropHandler += this.onDrop;
        }
    }

    private void onAssign(InventoryItem inventoryItem, int slot) {
        if(inventoryItem == null) {
            this.quickSlots[slot].Clear();
        }
        else {
            QuickSlotGUI quickSlot = this.quickSlots[slot];
            quickSlot.SetItem(inventoryItem.Data);
        }
    }

    private void onEquip(InventoryItem inventoryItem, int slot) {
        if(this.equippedId != slot && this.equippedId > -1) {
            this.quickSlots[equippedId].Background.color = new Color(0, 0, 0, 0.5f);
        }

        this.equippedId = slot;

        QuickSlotGUI quickSlot = this.quickSlots[slot];
        quickSlot.Background.color = new Color(0, 0, 0, 0.75f);
    }

    private void onUnequip(int slot) {
        this.quickSlots[equippedId].Background.color = new Color(0, 0, 0, 0.5f);
        this.equippedId = -1;
    }
}
