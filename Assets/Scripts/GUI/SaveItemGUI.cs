﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SaveItemGUI : MonoBehaviour
{
    public TextMeshProUGUI SaveName;
    public Button PlayButton;
    public Button RemoveButton;

    public void SetName(string saveName) {
        SaveName.text = saveName;
    }
}
