﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudGUI : MonoBehaviour
{
    public Slider HealthBar;

    private void Awake() {
        Player.onChangeHealth += this.onChangeHealth;
        Player.onChangeMaxHealth += this.onChangeHealth;

        Player player = FindObjectOfType<Player>();
        if(player) {
            HealthBar.value = (float)player.GetHealth() / (float)player.GetMaxHealth();
        }
    }

    private void onChangeHealth(int health, int oldHealth) {
        Player player = FindObjectOfType<Player>();
        if(player) {
            HealthBar.value = (float)player.GetHealth() / (float)player.GetMaxHealth();
        }
    }
}
