using UnityEngine;
using TMPro;

public class FocusGUI : MonoBehaviour {
    private TextMeshProUGUI focusText;
    private int layerMask;

    private void Awake() {
        this.focusText = GetComponent<TextMeshProUGUI>();
        this.layerMask = 1 << LayerMask.NameToLayer("Player");
        this.layerMask = ~(this.layerMask);
    }

    private void Update() {
        RaycastHit hit;
        if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 5f, this.layerMask)) {
            string tag = hit.collider.tag;
            switch(tag) {
                case "WorldItem":
                    {
                        WorldItem worldItem = hit.collider.GetComponent<WorldItem>();
                        focusText.SetText(worldItem.Data.Name + " (" + worldItem.Amount + ")");
                    }
                    break;
                case "WorldResource":
                    {
                        WorldResource worldResource = hit.collider.gameObject.GetComponentInParent<WorldResource>();
                        if(worldResource == null)
                            worldResource = hit.collider.gameObject.GetComponent<WorldResource>();
                        focusText.SetText(worldResource.Name + " (Destruction: " + worldResource.Health + ")" );
                    }
                    break;
                case "Untagged":
                    focusText.SetText("");
                    break;
            }
        }
        else 
            focusText.SetText("");
    }
}