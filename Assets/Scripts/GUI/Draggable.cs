using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public event Action<PointerEventData> OnBeginDragHandler;
	public event Action<PointerEventData> OnDragHandler;
	public event Action<PointerEventData, bool> OnEndDragHandler;

    public DropArea Area;

    private Vector3 position;

    private void Awake() {
        
    }

    public void OnDrag(PointerEventData eventData) { 
        OnDragHandler?.Invoke(eventData);
        transform.position = Input.mousePosition;
    }
    public void OnBeginDrag(PointerEventData eventData) { 
        OnBeginDragHandler?.Invoke(eventData);
        position = transform.position;
    }
    public void OnEndDrag(PointerEventData eventData) { 
        List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventData, results);

        DropArea dropArea = null;

        foreach(RaycastResult result in results) {
            dropArea = result.gameObject.GetComponent<DropArea>();
            if(dropArea != null) {
                break;
            }
        }

        transform.position = position;
        Area = dropArea;

        if (dropArea != null) {
            dropArea.Drop(this);
            OnEndDragHandler?.Invoke(eventData, true);
		}
        else {
            transform.position = position;
            OnEndDragHandler?.Invoke(eventData, false);
        }

        Area = null;
    }
}
