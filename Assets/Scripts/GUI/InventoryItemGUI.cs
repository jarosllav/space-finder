﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.EventSystems;

public class InventoryItemGUI : MonoBehaviour, IPointerClickHandler
{   
    // Public fields
    [Header("UI elements")]
    public Image Image;
    public TextMeshProUGUI Count;

    [System.NonSerialized] public InventoryItem Item;

    // Private fields
    private Draggable draggable;
    private DropArea dropArea;

    // Unity methods
    private void Awake() {
        dropArea = gameObject.AddComponent<DropArea>();
        dropArea.OnDropHandler += OnDrop;
        draggable = transform.GetComponentInChildren<Draggable>();
    }

    // Public methods
    public void Hide() {
        Image.gameObject.SetActive(false);
        Count.gameObject.SetActive(false);
    }

    public void Show() {
        Image.gameObject.SetActive(true);
        Count.gameObject.SetActive(true);
    }

    public void Clear() {
        Hide();
        draggable = null;
    }

    public void OnDrop(Draggable newDraggable) {
        if(draggable != newDraggable) {
            InventoryItemGUI invItem = newDraggable.transform.GetComponentInParent<InventoryItemGUI>();
            if(invItem != null) {
                PlayerInventory inventory = FindObjectOfType<Player>().GetPlayerInventory();

                if(draggable == null) {
                    this.Item = invItem.Item;
                    this.Image.sprite = invItem.Image.sprite;
                    this.Count.SetText(invItem.Count.text);

                    this.Show();
                    invItem.Clear();
                }
                else {
                    InventoryItem item = this.Item;
                    Sprite sprite = this.Image.sprite;
                    string count = this.Count.text;

                    this.Item = invItem.Item;
                    this.Image.sprite = invItem.Image.sprite;
                    this.Count.SetText(invItem.Count.text);

                    invItem.Item = item;
                    invItem.Image.sprite = sprite;
                    invItem.Count.SetText(count);
                }

                int slot = invItem.Item.Slot;
                int newSlot = transform.GetSiblingIndex();
                inventory.MoveItem(slot, newSlot);
            }

            draggable = newDraggable;
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        PlayerInventory inventory = FindObjectOfType<Player>().GetPlayerInventory();
        if(inventory == null)
            return;

        if((int)eventData.button == 0) {
            //inventory.GetQuickbar().AssignSlot(this.Item, 0);
        }
        else if((int)eventData.button == 1) {
            inventory.ConsumeItem(this.Item.Data);
        }
    }
}
