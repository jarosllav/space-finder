﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Planet"
{
    Properties
    {
        _Color ("Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            //Tags { "LightMode"="ForwardBase" }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

            struct app {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                nointerpolation fixed4 color : COLOR;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : TEXCOORD1;
                fixed4 diff : COLOR0;
                float4 worldPos : TEXCOORD2;
                fixed4 biome : COLOR1;
            };

            // Properties
            fixed4 _Color;

            v2f vert (app v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);

                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;

                o.biome = v.color;

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float view = dot(normalize(i.worldPos), float3(0,1,0));
                half4 color = i.biome;
                color *= i.diff;

                return color;
            }
            ENDCG
        }
    }
}
