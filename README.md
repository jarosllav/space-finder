# space-finder

Prototype of survival game with procedural world generation.

#### Screens
![alt](media/Capture1.PNG)
![alt](media/Capture2.PNG)
![alt](media/Capture3.PNG)

#### Video
[![alt](https://img.youtube.com/vi/FnwYP8DIvzI/0.jpg)](https://www.youtube.com/watch?v=FnwYP8DIvzI)
